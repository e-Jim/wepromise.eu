<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
	
	
	$this->load->library('geoip');

			$gi = geoip_open('./assets/GeoIP.dat', GEOIP_MEMORY_CACHE);
	
			$country = geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']);
		
			//$country = geoip_country_code_by_addr($gi, '199.66.81.83'); 
			
			geoip_close($gi);
		
			$my_countries = array('AL','AD','AM','AT','BY','BE','BA','BG','CH','CY','CZ','DE','DK','EE','ES','FO','FI','FR','GB','GE','GI','GR','HU','HR','IE','IS','IT','LT','LU','LV','MC','MK','MT','NO','NL','PO','PT','RO','RU','SE','SI','SK','SM','TR','UA','VA');

		/*
		$this->benchmark->mark('code_start');
		
		$this->db->order_by('ville_nom_reel','asc');
		$this->db->select('ville_id, ville_nom_reel, ville_nom_soundex, ville_nom_metaphone, ville_code_postal');
		$query = $this->db->get('villes_france');
		$ret=array();
	
		foreach ($query->result() as $row){
			$ar_ret['id'] =  $row->ville_id;
			$ar_ret['tokens'] =array($row->ville_code_postal,$row->ville_nom_reel);
			$ar_ret['value'] = $row->ville_nom_reel;
			$ret[] = $ar_ret;
			unset($ar_ret);
		}


		$data['json_cities'] = $ret;	
		$this->benchmark->mark('code_end'); 
		unset($ret);


		$this->db->select('ville_circoeu');
		$this->db->group_by('ville_circoeu');
		$query = $this->db->get('villes_france');
		
		//$data['json'] = $query->result();
		foreach ($query->result() as $row){
			
			$ret[] = $row->ville_circoeu;
		
		}	
		$data['json_districts'] = $ret;	
	*/
		$this->load->view('welcome_message');


//echo $this->benchmark->elapsed_time('code_start', 'code_end');
	}
	function results($search){
	
		$this->db->limit(10);
		$this->db->like('ville_nom_reel',$search);
		$this->db->or_like('ville_code_postal',$search);
		//$this->db->order_by('ville_nom_reel','asc');
		$this->db->select('ville_id, ville_nom_reel, ville_nom_soundex, ville_nom_metaphone, ville_code_postal');
		$query = $this->db->get('villes_france');
		$ret=array();
	
		foreach ($query->result() as $row){
			$ar_ret['id'] =  $row->ville_id;
			$ar_ret['tokens'] =array($row->ville_code_postal,$row->ville_nom_reel);
			$ar_ret['value'] = $row->ville_nom_reel.', France';
			$ret[] = $ar_ret;
			unset($ar_ret);
		}


		//$data['json_cities'] = $ret;	

		$data['json'] =$ret;
		
		$this->load->view('results',$data);

		}
	function districts(){
	$this->output->cache(10);
		$this->db->select('ville_circoeu');
		$this->db->group_by('ville_circoeu');
	//	$this->db->limit(2);
		$query = $this->db->get('villes_france');
		
		//$data['json'] = $query->result();
		foreach ($query->result() as $row){
			
			$ret[] = $row->ville_circoeu;
		
		}
		//$ret[] = 'cacat';
		$data['json'] =$ret ;
		$this->load->view('results',$data);

		}
		
function readExcel()
	{
		$this->load->library('csvreader');
		$result =   $this->csvreader->parse_file('./uploads/orase.csv');

		$data['csvData'] =  $result;
		
		foreach($result as $columns){
			foreach($columns as $column=>$value){		
				if($column=='name'){
						$this->db->insert('cities',array('name'=>$value));
						$city_id = $this->db->insert_id();
					}
				if($column=='districts'){
					$districts_array = array();
					$districts = explode('##',$value);
					if(count($districts)>0){
						$nr_districts = 0;
						foreach($districts as $district){
							$this->db->where('name',$district);
							$query = $this->db->get('districts');
							
							if($query->num_rows()==0) {
								$this->db->insert('districts',array('name'=>$district));
								$district_id = $this->db->insert_id();
								$this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));
								$districts_array[$nr_districts] = $district_id;
								$nr_districts++;
								
							}
							else{
								$district_id = $query->row()->id;
								$this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));
								$districts_array[$nr_districts] = $district_id;
								$nr_districts++;
							}
							 
						}
					}
					
				}
				if($column=='zipcodes'){
					
					$zipcodes = explode('##',$value);
					if(count($zipcodes)>0){
					var_dump($zipcodes);
						$district_key = 0;
						foreach($zipcodes as $zips_by_district_separated_by_slash){
							if($zips_by_district_separated_by_slash!='no_zipcodes') {
								$zip_for_district = explode('|',$zips_by_district_separated_by_slash);
								foreach($zip_for_district as $the_zip){
									$this->db->insert('zipcodes',array('zip'=>$the_zip,'city_id'=>$city_id));
									$zipcode_id = $this->db->insert_id();
									$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$districts_array[$district_key]));
								}
							}
							$district_key++; 
						}	 
					}
				
				}  
				unset($district_array);
			}	
		}
		var_dump($data);
		$this->load->view('view_csv', $data);  
	}
		
	function cities($search){
		
		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->like('cities.name',$search,'after');
		$this->db->limit(10); 
		$query = $this->db->get('cities');
		$ret=array(); 
	
		foreach ($query->result() as $row){
			$ar_ret['id'] 	  =  $row->city_id;
			$ar_ret['tokens'] = $row->city_name;
			$ar_ret['value']  = $row->city_name.', '.$row->country_name;
			$ret[] = $ar_ret;
			unset($ar_ret);
		}


		//$data['json_cities'] = $ret;	
		/*
		if(count($ret)==0){
			$ar_ret['id'] 	  =  0;
			$ar_ret['tokens'] = "no_results";
			$ar_ret['value']  = "No results";
			$data['json'] = "No results";
		}
		*/ 	
		
		$data['json'] = $ret;
		$this->load->view('results',$data);	
			
	}
		 
		function zipcodes ($search){
		
			$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,zipcodes.zip,zipcodes.id as zip_id');
			$this->db->join('cities','cities.id=zipcodes.city_id');
			$this->db->join('countries','countries.id=cities.country_id');
			$this->db->like('zipcodes.zip',$search,'after');
			//$this->db->order_by('city_name','asc');
			$this->db->limit(10);
			$query = $this->db->get('zipcodes');
			$ret=array();	


			foreach ($query->result() as $row){
				$ar_ret['id'] 	  =  $row->zip_id;
				$ar_ret['tokens'] = $row->zip;
				$ar_ret['value']  = $row->zip.', '.$row->city_name.', '.$row->country_name;
				$ret[] = $ar_ret;
				unset($ar_ret);
			}


			//$data['json_cities'] = $ret;	

			//if(count($ret)>0)
				$data['json'] = $ret;
			//else $data['json'] = "No results";
			
			$this->load->view('results',$data);				
		}
		
		function search_results($search){
	
			$json=array(); 
			
			
			$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id');
			$this->db->join('countries','countries.id=cities.country_id');
			$this->db->like('cities.name',$search,'after');
			$this->db->limit(10); 
			$query = $this->db->get('cities');
			//echo $this->db->last_query();
		
			foreach ($query->result() as $row){
				$ar_ret['city_id'] 	  =  $row->city_id;
				$ar_ret['tokens'] = $row->city_name;
				$ar_ret['value']  = $row->city_name.', '.$row->country_name;
				$json[] = $ar_ret;
				unset($ar_ret);
			}		

			$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,zipcodes.zip,zipcodes.id as zip_id');
			$this->db->join('cities','cities.id=zipcodes.city_id');
			$this->db->join('countries','countries.id=cities.country_id');
			$this->db->like('zipcodes.zip',$search,'after');
			//$this->db->order_by('city_name','asc');
			$this->db->limit(10);
			$query = $this->db->get('zipcodes');

			foreach ($query->result() as $row){
				$ar_ret['zip_id'] 	  	=  $row->zip_id;
				$ar_ret['tokens'] 		=  $row->zip;
				$ar_ret['value'] 		=  $row->zip.', '.$row->city_name.', '.$row->country_name;
				$json[]					=  $ar_ret;
				unset($ar_ret);
			}			

			
			
			if(count($json)==0){ 
				
				$ar_ret['id'] 	  =  0;
				$ar_ret['tokens'] = "no_results";
				$ar_ret['value']  = "No results";
				$json[]			  = $ar_ret;
				
			}
			$data['json'] = $json;
			$this->load->view('results',$data);	
			
		}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */