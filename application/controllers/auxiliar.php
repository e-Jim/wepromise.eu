<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auxiliar extends CI_Controller {
	 
	function __construct(){
		parent::__construct();
		$this->load->model('edri_front');
	}
	function send_manual_mail(){
		$this->edri_front->send_confirm_promise_email_manual();
		echo "mail trimis";
	} 
	function handle_vote_promise(){
		if($this->input->is_ajax_request()){ //make sure everything is allright
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('name', 'Name', 'xss_clean|required');
			$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|email');
			$this->form_validation->set_rules('country', 'Country', 'xss_clean|required');
			
			if($this->form_validation->run()){
				$data = new stdClass();
				
				$data->name = $this->input->post('name');
				$data->email = $this->input->post('email'); 
				$data->country_code = $this->input->post('country');
				
				$data->receive_news = ($this->input->post('subscribe') != false ? "yes" : "no");
				
				
				if($this->edri_front->register_vote_promise($data)) {
					
					$this->edri_front->send_confirm_promise_email($data->email, $data->name);
					echo json_encode(array('state'=>'true'));
				}
				else echo json_encode(array('state'=>'false_email'));
			} else {
			
				$email_required = ($this->input->post('email')=="")?'false':'true';
				$name_required = ($this->input->post('name')=="")?'false':'true';
				$country_required = ($this->input->post('country')=="")?'false':'true';
				echo json_encode(array('state'=>'false','name_required'=>$name_required,'email_required'=>$email_required,'country_required'=>$country_required));
			}
			
		} else {
			echo "you are not supposed to see this";
		}
	} 
	
	
	function handle_confirm_vp($code){
		$this->load->library('session');

		
		$query =  $this->db->get_where('flash_codes', array('code'=>$code));
		
		
		if($query->num_rows() > 0){
			$row = $query->row();
			
			$this->db->where('id', $row->data);
			$this->db->update('vote_promises',array('validated'=>'yes'));
			
			$this->db->where('code',$code);
			$this->db->update('flash_codes', array('expired'=>'1'));
			
			$this->session->set_flashdata('vp_confirmation_response', "success");
		} else {
			$this->session->set_flashdata('vp_confirmation_response', "fail");
		}
		
		redirect('');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */