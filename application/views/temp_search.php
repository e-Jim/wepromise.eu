﻿<!DOCTYPE html>
<html>
	<head>
		<title>Bootstrap 101 Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="<?=base_url();?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url();?>assets/frontend/css/main.css" rel="stylesheet">


<!-- fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,700italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Leckerli+One' rel='stylesheet' type='text/css'>

<!-- /fonts -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head> 
	<body >

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=402166953153783";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>



 



		<div class="container" id="header">
			<div class="row">
				<div class="col-sm-3">
					<a href="#"><img id="logo" alt="logo" src="<?=base_url()?>assets/frontend/img/logo.png" /></a>


					<div class="pull-right   visible-xs " >
						<div class="lang_switch clickable">
							EN

							<div class="options">
								<a href="2">English</a>
								<hr>
								<a href="2">German</a>
								<hr>
								<a href="2">Portughese</a>
							</div>


						</div>
					</div>

				</div>
				
				<div class="pull-right col-sm-1 hidden-xs" >
					<div class="lang_switch clickable">
						EN

						<div class="options">
							<a href="2">English</a>
							<hr>
							<a href="2">German</a>
							<hr>
							<a href="2">Portughese</a>
						</div>


					</div>
				</div>


				<div class="web_menu col-sm-7  hidden-xs ">
					<a class="item active" href="#">Link one</a><!--
				 --><a class="item" href="#">Link three</a><!--
				 --><a class="item" href="#">Link four</a><!--
				 --><a class="item" href="#">Lorem link</a>
				</div>




			</div>
		</div>

		<div class="results_page">

			<div   class="promise_form_container ">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-text">
							

								Promise to vote for any candidate who supports the <span class="em1">10 point Charter of</span>  
								<span class="em2">
									Digital Rights.

								</span>
							</div>
						</div>

					</div>


					<div class="row">
						<div class="col-sm-3">
							<input type="text" placeholder="Name" />
						</div>
						<div class="col-sm-3">
							<input type="text" placeholder="E-mail" />
						</div>
						<div class="col-sm-3">
							<div class="pseudo_select">
								<input type="hidden" value="" />
								<span class="screen">Country</span>
								
								<div class="options">
									<div data-value="">Country</div>
									<div data-value="at">Austria</div>
									<div data-value="be">Belgium</div>
									<div data-value="bg">Bulgaria</div>
									<div data-value="hr">Croatia</div>
									<div data-value="cy">Cyprus</div>
									<div data-value="cz">Czech Republic</div>
									<div data-value="dk">Denmark</div>
									<div data-value="ee">Estonia</div>
									<div data-value="fi">Finland</div>
									<div data-value="fr">France</div>
									<div data-value="de">Germany</div>
									<div data-value="gr">Greece</div>
									<div data-value="hu">Hungary</div>
									<div data-value="ie">Ireland</div>
									<div data-value="it">Italy</div>
									<div data-value="lv">Latvia</div>
									<div data-value="lt">Lithuania</div>
									<div data-value="lu">Luxembourg</div>
									<div data-value="mt">Malta</div>
									<div data-value="nl">Netherlands</div>
									<div data-value="pl">Poland</div>
									<div data-value="pt">Portugal</div>
									<div data-value="ro">Romania</div>
									<div data-value="sk">Slovakia</div>
									<div data-value="si">Slovenia</div>
									<div data-value="es">Spain</div>
									<div data-value="se">Sweden</div>
									<div data-value="gb">United Kingdom</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<input class="clickable" type="submit" value="Promise to vote" />
							
								<div class="c_tooltip reversed">
										<span class="c_close">&times;</span>
									
										<span class="title">9899</span>
										<span class="label">fellow citizens <br/> promised to vote</span>
										<span class="nub">
											<span class="nub_shadow"></span>
										</span>
								</div>
						</div>


					</div>

				</div>
			</div>
			
			
			
			
			
			<div class="search_strip">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="search_quote">
								<strong>212 <?=$content->results_for?></strong>&nbsp; &ldquo;<span class="quote">Aetolia-Acarnia, Greece </span>&rdquo;  
								
							</div>
						</div>
						
						<!--div class="col-md-1 visible-md visible-lg">  </div--> <!-- offset hack -->
						 
						<div class="col-md-4 relative_positioned">
							<div>
								<div class="focusable_child" id="input_wrapper">
									<input type="text" class="typeahead" placeholder="Search by City , ZIP Code or Candidate" />
									<input type="submit" value="" />

								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			
			
			<div id="results_container">
			
<!-- ########################################################################################  set of 4 results -->			
				<div class="container">
					<div class="row">
						<div class="col-md-6">
						
							<div class="row">
								<div class="col-sm-6">
									<div class="result">
										<div  class="title flag-gb">
											Yorkshire<br/>
											Yorkshire and the Humber<br />
											United Kingdom
										</div>
										
										<div class="name">John </div>
										<div class="party">
											Socialist Party<br />
											<a href="#" >www.greekliberals.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								
								</div>
								<div class="col-sm-6">
								
									<div class="result">
										<div  class="title flag-de">
											Köln<br/>
											North Rhine-Westphalia<br />
											Deutschland
										</div>
										
										<div class="name">Johann Fuhrmann </div>
										<div class="party">
											Liberal Alliance<br />
											<a href="#" >www.liberal-alliance.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								
								
								</div>
							
							</div>
						
						
						</div>
						
						<div class="col-md-6">
							<div class="row">
								<div class="col-sm-6">
									<div class="result">
										<div  class="title flag-gb">
											Yorkshire<br/>
											Yorkshire and the Humber<br />
											United Kingdom
										</div>
										
										<div class="name">John </div>
										<div class="party">
											Socialist Party<br />
											<a href="#" >www.greekliberals.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
								
									<div class="result">
										<div  class="title flag-de">
											Köln<br/>
											North Rhine-Westphalia<br />
											Deutschland
										</div>
										
										<div class="name">Johann Fuhrmann </div>
										<div class="party">
											Liberal Alliance<br />
											<a href="#" >www.liberal-alliance.net</a>
										</div>
												
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								</div>
							</div>
						</div>
						
						
					</div>
					
					
					<div class="result_section_delimiter row">
						<?=$content->results_section?> 1
					</div>
					
				</div>
				
				
<!-- ######################################################################################## -->				
				<div class="container">
					<div class="row">
						<div class="col-md-6">
						
							<div class="row">
								<div class="col-sm-6">
									<div class="result">
										<div  class="title flag-gb">
											Yorkshire<br/>
											Yorkshire and the Humber<br />
											United Kingdom
										</div>
										
										<div class="name">John </div>
										<div class="party">
											Socialist Party<br />
											<a href="#" >www.greekliberals.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								
								</div>
								<div class="col-sm-6">
								
									<div class="result">
										<div  class="title flag-de">
											Köln<br/>
											North Rhine-Westphalia<br />
											Deutschland
										</div>
										
										<div class="name">Johann Fuhrmann </div>
										<div class="party">
											Liberal Alliance<br />
											<a href="#" >www.liberal-alliance.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								
								
								</div>
							
							</div>
						
						
						</div>
						
						<div class="col-md-6">
							<div class="row">
								<div class="col-sm-6">
									<div class="result">
										<div  class="title flag-gb">
											Yorkshire<br/>
											Yorkshire and the Humber<br />
											United Kingdom
										</div>
										
										<div class="name">John </div>
										<div class="party">
											Socialist Party<br />
											<a href="#" >www.greekliberals.net</a>
										</div>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, tum nascetur ridiculus mus. </p>
										<p>Donec edis ullamcorper nulla non metus nerim auctor fringilla. Duis mollis, est non commodo luctus.</p>
										
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
								
									<div class="result">
										<div  class="title flag-de">
											Köln<br/>
											North Rhine-Westphalia<br />
											Deutschland
										</div>
										
										<div class="name">Johann Fuhrmann </div>
										<div class="party">
											Liberal Alliance<br />
											<a href="#" >www.liberal-alliance.net</a>
										</div>
												
										<div class="social">
											<div class="row">
												<div class="col-xs-3 " >
													<a class="tw" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >

													<a class="fb" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_center" >
													<a class="gplus" href="#"></a>
												</div>
												<div class="col-xs-3 text_align_right" >
													<a class="mail" href="#"></a>
												</div>
											</div>
										</div>
									</div>
								
								</div>
							</div>
						</div>
						
						
					</div>
					
					
					<div class="result_section_delimiter row">
						Results section 2
					</div>
					
				</div>	
				
				
				
				
				
				
			</div>
			
		</div>
		
		<div id="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="title">Spread the news:</div>
					</div>


					<div class="col-sm-4"> 
						<a href="#" class="footer_sm footer_twitter"></a><!--


	 
					 --><a href="#" class="footer_sm footer_facebook"></a><!-- 


					 --><a href="#" class="footer_sm footer_gplus"></a><!--



					 --><a href="#" class="footer_sm footer_mail"></a>
					</div>

					<div class="col-md-3">
						
						<a href="#" class="clickable green_button">Download  Support Kit</a>
					</div>
				</div>
			</div>

		</div>

		<div id="footer_bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">CC (2013). This website is published under a creative commons licence.</div>
				</div>
			</div>


		</div>
		



		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?=base_url();?>assets/frontend/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>


		
		
		
		
		
		

		<script type="text/JavaScript">
			
			
			
			$(function(){
				//select
				$('.pseudo_select').click(function(event){
					if(! $(this).children('.options').is(":visible")) {
						event.stopPropagation();
						$(this).children('.options').show();
						$(this).addClass('focused');
					} 
				});
				
				
				//
				$('.focusable_child').find('input').focus(function(){
					$(this).closest('.focusable_child').addClass('focused');
				});
				
				$('.focusable_child').find('input').blur(function(){
					$(this).closest('.focusable_child').removeClass('focused');
				});
				
				$('.pseudo_select .options > div').click(function(event){
				
				
					var value = $(this).attr('data-value');
					
					var select = $(this).closest('.pseudo_select');
					
					select.children('input').val(value);
					select.children('.screen').html($(this).html());
					
					select.find('.options').hide();
					select.removeClass('focused');
					
					select.removeClass('selected');
					if(value != ""){
						select.addClass('selected'); 
					}
					
					event.stopPropagation();
					
				});
			
				$('body').click(function(){
					$('.pseudo_select .options').hide();
					$('.pseudo_select').removeClass('focused');
				});
			
			
			
				//resize the map

				

				
			
				$('.clickable').mousedown(function(){
					$(this).addClass('active');
				});

				$('body').mouseup(function(){
					$('.clickable.active').removeClass('active');
				});

				$('body').click(function(){
					$('.lang_switch.on').removeClass('on').children('.options').fadeOut('fast');
				});


				$('.lang_switch').click(function(event){
					event.stopPropagation();
					//event.preventDefault();


					if($(this).hasClass('on') ){
						return;
					}

					$(this).addClass('on');

					var el_height = $(this).children('.options').height() + 20;

					$(this).children('.options').css({ 'display':'block', 'height':'0px','opacity':'0' }).stop().animate(
						{
							opacity: 1,
							height: el_height
						}, 300,
						function(){
						}
					);
				});




				//tooltip
				
				
				$(window).scroll(function(){
					if($('.c_tooltip').offset().top - $(window).scrollTop() < 200 &&  !$('.c_tooltip').is(':visible')  ) {
						$('.c_tooltip').fadeIn('fast');
					}
				});
				
				$('.c_tooltip').children('.c_close').click(function(){
					$(this).parent().css('visibility','hidden');
				});
				


				/*ssearch whatever*/





						var myTypeahead = $('.typeahead').typeahead(
							 [
								{
									remote: {
										url: 'http://webfoo.ro/projects/parlamentari/site/welcome/cities/',
										replace: function () {
											var q = 'http://webfoo.ro/projects/parlamentari/site/welcome/cities/';
											if ($('.typeahead').val()) {
												q += encodeURIComponent($('.typeahead').val());
											}
											return q;
										},
										filter: function(parsedResponse) {

											var dataset = [];
											for (i = 0; i < parsedResponse.length; i++) {
											  dataset.push({
												value: parsedResponse[i].value,
												tokens: parsedResponse[i].tokens
											  });
											}
											if (parsedResponse.length == 0) {
											  dataset.push({
												value: "No results" 
											  });
											}
											return dataset;
										},
									},

									cache: false,
									limit: 10
						        },
								{
									remote: {
										url: 'http://webfoo.ro/projects/parlamentari/site/welcome/zipcodes/',
										replace: function () {
											var q = 'http://webfoo.ro/projects/parlamentari/site/welcome/zipcodes/';
											if ($('.typeahead').val()) {
												q += encodeURIComponent($('.typeahead').val());
											}
											return q;
										}
									},
									cache: false,
									limit: 10
						        }		
							]);	


							// myTypeahead.on('typeahead:selected',function(evt,data){
							// 	console.log('data==>' + data.id); //selected datum object
							// });


			});


		</script>




	</body>
</html>