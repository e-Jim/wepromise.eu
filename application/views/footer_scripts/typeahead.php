
	<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>
	<script type="text/javascript">

	$( document ).ready(function() {

	var myTypeahead = $('.typeahead').typeahead(

		{
			remote: {
				url: '<?=base_url()?>pages/search_results/',
				replace: function () {
				
					var q = '<?=base_url()?>pages/search_results/';
					if ($('.typeahead').val()) {
						q += encodeURIComponent($('.typeahead').val());
					}
					return q;
				
				}/*
				,
				
				filter: function(parsedResponse) { 
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens
					  });
					}
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No results" 
					  });
					}
					return dataset;
				} 
				*/
				 
			}, 
 
			cache: false,
			limit: 15
        } 
	); 
		myTypeahead.on('typeahead:selected',function(evt,data){
			console.log('zip_id==>' + data.zip_id); //selected datum object
			console.log('city_id==>' + data.city_id); //selected datum object	
			console.log('district_id==>' +data.district_id);
			console.log('tokens==>' +data.tokens);
			console.log('value==>' +data.value);
			
			if(typeof data.zip_id!=='undefined') window.location.href='<?=base_url()?><?=$this->uri->segment(1)?>/zip/'+data.zip_id;
			if(typeof data.city_id!=='undefined') window.location.href='<?=base_url()?><?=$this->uri->segment(1)?>/city/'+data.city_id;
			if(typeof data.district_id!=='undefined') window.location.href='<?=base_url()?><?=$this->uri->segment(1)?>/district/'+data.district_id;
		});
	}); 
		
</script>