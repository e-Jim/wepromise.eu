<script type="text/javascript">
		
$(function(){	
		$('.focusable_child').find('input').focus(function(){
			$(this).closest('.focusable_child').addClass('focused');
		});
		
		$('.focusable_child').find('input').blur(function(){
			$(this).closest('.focusable_child').removeClass('focused');
		});
		
		
		
			
		$('.pseudo_select .options > div').click(function(event){
		
		
			var value = $(this).attr('data-value');
			
			var select = $(this).closest('.pseudo_select');
			
			select.children('input').val(value);
			select.children('.screen').html($(this).html());
			
			select.find('.options').hide();
			select.removeClass('focused');
			
			select.removeClass('selected');
			if(value != ""){
				select.addClass('selected');
			}
			
			event.stopPropagation();
			
		});
	
		$('body').click(function(){
			$('.pseudo_select .options').hide();
			$('.pseudo_select').removeClass('focused');
		});
	



		$('.pseudo_select').click(function(event){
			if(! $(this).children('.options').is(":visible")) {
				event.stopPropagation();
				$(this).children('.options').show();
				$(this).addClass('focused');
			} 

		});


		$('.clickable').mousedown(function(){
			$(this).addClass('active');
		});

		$('body').mouseup(function(){
			$('.clickable.active').removeClass('active');
		});

		$('body').click(function(){
			$('.lang_switch.on').removeClass('on').children('.options').fadeOut('fast');
		});


		$('.lang_switch').click(function(event){
			event.stopPropagation(); 
			if($(this).hasClass('on') ){
				$(this).removeClass('on').children('.options').fadeOut('fast');
				return;
			}

			$(this).addClass('on');

			var el_height = $(this).children('.options').height() + 20;

			$(this).children('.options').css({ 'display':'block', 'height':'0px','opacity':'0' }).stop().animate(
				{
					opacity: 1,
					height: el_height
				}, 300,
				function(){
				}
			);
		});



		$(window).scroll(function(){
			if($('.c_tooltip').offset().top - $(window).scrollTop() < 200 &&  !$('.c_tooltip').is(':visible')  ) {
				$('.c_tooltip').fadeIn('fast');
			}
		});
	//	$('.c_tooltip').css('visibility','hidden');
		$('.c_tooltip').click(function(){
			$(this).css('visibility','hidden');
		});
		
		
		$('#mobile_menu').click(function(){
			var _box = $('#mobile_menu_box');
			
			if( _box.hasClass('in_transition') ) {
				return;
			}
			
			if( _box.hasClass('open')){
				
				_box.addClass('in_transition').css({'left': 0, 'opacity':1}).animate({
					left: -200,
					opacity: 0
				}, 500, function(){
					_box.removeClass('in_transition');
					_box.removeClass('open');
				});
				
			} else {
				_box.addClass('open').addClass('in_transition').css({'left': -200, 'opacity': 0}).animate({
					left: 0,
					opacity: 1
				}, 500, function(){
					_box.removeClass('in_transition');
				});
			}
			
		});
		
		
		$('#close_menu_btn').click(function(){
			var _box = $('#mobile_menu_box');
			
			if( _box.hasClass('in_transition')  || !_box.hasClass('open') ) {
				return;
			}
			
			_box.addClass('in_transition').css({'left': 0, 'opacity':1}).animate({
				left: -200,
				opacity: 0
			}, 500, function(){
				_box.removeClass('in_transition');
				_box.removeClass('open');
			});
		});

});



</script>

<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.form.js"></script>

<script type="text/javascript">

				 

	$(function(){ 
	

		//promise to vote
		var optionsQ = {  
				dataType	: 		'json',
				success		:	showResponseCool
			}
		$('#promise_to_vote_form').ajaxForm(optionsQ);
			 
		function showResponseCool(data){
		
			if(data.state=="true"){
				$('.promisse_form').hide();
				$('.please_validate_email').fadeIn('slow');
			}
			if(data.state=="false"){
				//$('.promisse_form').hide();
				if(data.name_required=='false'){
					$('.name_required').css("display","block");	console.log('test');}
				if(data.email_required=='false')
					$('.email_required').css("display","block");	
				if(data.country_required=='false')
					$('.country_required').css("display","block");						
				$('.message_error').fadeIn().delay(1500);	
				$('.message_error').fadeOut().delay(1500);			
				
				$('.name_required').delay(3500).fadeOut();				
				$('.email_required').delay(3500).fadeOut();		 			
				$('.country_required').delay(3500).fadeOut();					
				//$('.promisse_form').show();
			

			}			
			if(data.state=="false_email"){
				//$('.promisse_form').hide();
				$('.message_error_email').fadeIn().delay(1500);	
				$('.message_error_email').fadeOut().delay(1500);				
				//$('.promisse_form').show();

			}
		}
	});
	
	$('#promise_to_vote_form label').click(function() {
			if($(this).children().children('#the_checkbox').hasClass('active')){
				$(this).children().children('#the_checkbox').removeClass('active');
				$(this).prev().attr('checked', 'checked');
				
			}
			else{
				$(this).children().children('#the_checkbox').addClass('active');
				$(this).prev().removeAttr('checked');
			}
	});	

	


</script>