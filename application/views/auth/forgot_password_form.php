<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/admin/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?=base_url()?>assets/admin/css/sb-admin.css" rel="stylesheet">
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'class'=>'form-control'
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>
		<div class="container">
		<div class="row"><div class="col-sm-12 text-center"><img src="<?=base_url()?>assets/frontend/img/logo.png"></div></div>
		<br/><br/>
		<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<?php echo form_open($this->uri->uri_string()); ?>
			<?php echo form_label($login_label, $login['id']); ?><br/>
			<?php echo form_input($login); ?>
			<br/>
				

			<?php echo form_submit('reset', 'Get a new password','class="btn btn-primary"'); ?>
				<div style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></div>
			<?php echo form_close(); ?>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?=base_url();?>assets/frontend/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>


	</body>
</html>