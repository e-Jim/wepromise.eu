<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/admin/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?=base_url()?>assets/admin/css/sb-admin.css" rel="stylesheet">
		<?php
		$login = array(
			'name'	=> 'login',
			'id'	=> 'login',
			'value' => set_value('login'),
			'maxlength'	=> 80,
			'size'	=> 30,
			'class'=>'form-control'
		);
		if ($login_by_username AND $login_by_email) {
			$login_label = 'Email or login';
		} else if ($login_by_username) {
			$login_label = 'Login';
		} else {
			$login_label = 'Email';
		}
		$password = array(
			'name'	=> 'password',
			'id'	=> 'password',
			'size'	=> 30,
			'class'=>'form-control'
		);
		$remember = array(
			'name'	=> 'remember',
			'id'	=> 'remember',
			'value'	=> 1,
			'checked'	=> set_value('remember'),
			'style' => 'margin:0;padding:0',
		);
		$captcha = array(
			'name'	=> 'captcha',
			'id'	=> 'captcha',
			'maxlength'	=> 8,
		);
		?>
		<div class="container">
		<div class="row"><div class="col-sm-12 text-center"><img src="<?=base_url()?>assets/frontend/img/logo.png"></div></div>
		<br/><br/>
		<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
		<?php echo form_open($this->uri->uri_string()); ?>
		<?php echo form_label($login_label, $login['id']); ?><br/>
		<?php echo form_input($login); ?><br/>
	
		<?php echo form_label('Password', $password['id']); ?><br/>
		<?php echo form_password($password); ?><br/>
		

		<?php echo form_checkbox($remember); ?>
		<?php echo form_label('Remember me', $remember['id']); ?> 
		<?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?><br/>
		<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?><br/>

		<?php echo form_submit('submit', 'Let me in','class="btn btn-primary"'); ?>
		<div style="color:red">
		<?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
		</div>
		<div style="color:red">
		<br/><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
		</div>
		<?php echo form_close(); ?>
		</div>
		</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?=base_url();?>assets/frontend/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>


	</body>
</html>