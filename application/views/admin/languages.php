<?=$header?>
	<div id="page-wrapper">
		<?php if($this->session->flashdata('errors')||$this->session->flashdata('success')):?>
			<div class="alert alert-dismissable <?=($this->session->flashdata('errors'))?"alert-danger":"alert-success"?>">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('errors')?>
				
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-lg-12">
				<h1>All languages</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i>All languages</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
			<!--
				<div class="row">
					<div class="col-lg-3"><input placeholder="Search" name="filter" class="form-control" id="filter-box" value="" maxlength="30" size="30" type="text"></div>
					<div class="col-lg-2"><input id="filter-clear-button" class="btn btn-success"  type="submit" value="Clear"/></div>
				</div>
				<br/>			 
			-->
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Language </th>
								<th>Show in dropdown </th>
								<th>Save</th>
							</tr>
						</thead>
						<tbody>
		
						<?php foreach($languages as $language):?>
							<form method="post" action="" enctype="multipart/form-data" />
								<tr>
									<td style="vertical-align:middle"><?=$language->name?></td>
									<td style="vertical-align:middle">
										<div id="label-switch" class="make-switch" data-on-label="ON" data-off-label="OFF">
											<input type="checkbox" <?=($language->status=='true')?'checked="checked"':'';?>>
										</div>					 
									</td>
									<td><?=form_submit('send','Save','class="btn btn-primary"')?></td>
								</tr>
								<?=form_hidden('status')?>
								<?=form_hidden('language_id',$language->id)?>
							</form>
						<?php endforeach;?>
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>