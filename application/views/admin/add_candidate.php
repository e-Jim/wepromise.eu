<?=$header?>
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=validation_errors()?>
				
			</div> 
		<?php endif;?>
		<div class="row">
			<div class="col-sm-12">
				<h1>Add candidate</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li><a href="<?=base_url()?>admin/candidates">Candidates</a></li>
				<li class="active"><i class="icon-file-alt"></i> Add candidate</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" enctype="multipart/form-data" />
					<div class="row">
						<div class="col-sm-4">					
							<?=form_label('Name','','class="form-control"')?><br/>
							<?=form_input('name',set_value('name'),'class="form-control"')?>
						</div>
						<div class="col-sm-4">
							<?=form_label('Website (Ex:http://www.website.com)','','class="form-control"')?> <br/>
							<?=form_input('website',set_value('website'),'class="form-control"')?> 
						</div>	
						<div class="col-sm-4">
							<?=form_label('Party name','','class="form-control"')?> <br/>
							<?=form_input('party_name',set_value('party_name'),'class="form-control"')?> 
						</div>											
					</div>
					<br/>			
					<div class="row">
						<div class="col-sm-3">					
							<?=form_label('Facebook','','class="form-control"')?><br/>
							<?=form_input('facebook',set_value('facebook'),'class="form-control"')?>
						</div>
						<div class="col-sm-3">
							<?=form_label('Twitter (Ex:http://www.twitter.com/username)','','class="form-control"')?> <br/>
							<?=form_input('twitter',set_value('twitter'),'class="form-control"')?> 
						</div>	
						<div class="col-sm-3">
							<?=form_label('Google+','','class="form-control"')?> <br/>
							<?=form_input('gplus',set_value('gplus'),'class="form-control"')?> 
						</div>							
						<div class="col-sm-3">
							<?=form_label('Youtube','','class="form-control"')?> <br/>
							<?=form_input('youtube',set_value('youtube'),'class="form-control"')?> 
						</div>											
					</div>
					<br/>						
					<div class="row">
		
						<div class="col-sm-12 candidate_description">
							<label>Candidate description (15 to 75 words)</label><br/>
							<ul class="nav nav-tabs">
							<?php $i=0;  foreach($languages as $language):?>
								<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
									<a data-toggle="tab" href="#<?=strtolower($language->name)?>">
										<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
									</a>
								</li>
							<?php $i++; endforeach;?>
							
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>">
									<br/>
									<textarea class="form-control" cols="4" style="height:300px" name="desc_<?=$language->id?>"><?=set_value('desc_'.$language->id)?></textarea>
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					
					</div> 
					<br/>  
					<div class="row">
						<div class="col-sm-4">		
							<label>Choose country</label><br/>
  
							<div class="example countries"> 
								<div class="demo">
								  <input class="typeahead" type="text" name="typeahead" placeholder="Country">
								  <input type="hidden" name="country_id" value="<?=set_value('country_id')?>" >
								</div>
							</div>						
						</div>
						<div class="col-sm-4">		 
							<div class="cities_suggest">
								<label>Choose city</label><br/>
	  
								<div class="example cities"> 
									<div class="demo">
									  <input class="typeahead" type="text" name="typeahead" placeholder="City">
									  <input type="hidden" name="city_id" value="<?=set_value('city_id')?>" >
									</div>
								</div>		
							</div>
						</div>
						<div class="col-sm-4">		
							<div class="districts_suggest">
							
								<label>Districts</label><br/>
								<div class="example districts"> 
									<div class="demo">
									  <!--<input class="typeahead" type="text" name="typeahead" placeholder="Districts">-->
									  <select name="district_id" class="districts_dropdown">
										<option>Please select district</option>
									  </select>
									</div>
								</div>	
								
							</div>
						</div>						
					</div>
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Candidate profile picture</label>
							<input type="file"  name="photo">
						</div>
					</div>
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<input class="btn btn-primary" type="submit" name="send" value="Add candidate">
						</div>
					</div>
				</form>
					

		 
			</div>  
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>