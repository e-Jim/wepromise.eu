<?=$header?> 
	<div id="page-wrapper">
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>		
		 
		<div class="row">
			<div class="col-sm-12">
				<h1>Videos</h1>
				<ol class="breadcrumb">
					<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
					<li class="active">Videos</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" >
				<div class="row">
				
					<div class="col-sm-6">
						<label>Name</label>
						<input type="text" class="form-control" name="name">
					</div>

					<div class="col-sm-6">
						<label>video code, after v=</label>
						<input type="text"  class="form-control" name="link">
					</div>
				</div>	
				<br/>				
				
				<div class="row">
					
					<div class="col-sm-12">
						<input class="btn btn-primary" type="submit" name="send" value="Add video">
					</div>
				</div>
				</form>
			</div>  
		</div>
		<br/>
		<div class="row">
			<?php  
				foreach($videos as $partner):		
			?>
			<div class="col-sm-4"> 
				<form method="post" action="">
					<label>Name</label>
					<input type="text" class="form-control" value="<?=$partner->name?>" name="name"><br/>
					<label>video code, after v=</label>
					<input type="text"  class="form-control" value="<?=$partner->link?>" name="link"><br/>
					<a onclick="return confirm('Do you want to delete <?=$partner->name?>?')"  class="btn btn-danger" href="<?=base_url()?>admin/delete_video/<?=$partner->id?>">Delete</a>
					<input type="hidden" name="video_id" value="<?=$partner->id?>">
					<input type="submit" class="btn btn-primary"  value="Save video" name="save_video">
				</form>
			</div>
			<?php endforeach;?>
		</div><!-- /#page-wrapper -->
<?=$footer?>