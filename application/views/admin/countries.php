<?=$header?>
	<div id="page-wrapper">
		<?php if($this->session->flashdata('errors')||$this->session->flashdata('success')):?>
			<div class="alert alert-dismissable <?=($this->session->flashdata('errors'))?"alert-danger":"alert-success"?>">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('errors')?>
				
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-lg-12">
				<h1>All countries</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i> All countries</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<!--
				<div class="row">
					<div class="col-lg-3"><input placeholder="Search" name="filter" class="form-control" id="filter-box" value="" maxlength="30" size="30" type="text"></div>
					<div class="col-lg-2"><input id="filter-clear-button" class="btn btn-success"  type="submit" value="Clear"/></div>
				</div>
				<br/>
				-->
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Country</th>
								<th>Language</th>
								<th>Import</th>
								<th>Add new city</th>
								<th>Save</th>
								<th>Clear imported data</th>
								
							</tr>
						</thead> 
						<tbody>
						<?php foreach($countries as $country):?>
						<form method="post" action="" enctype="multipart/form-data" />
							<tr>
								<td style="vertical-align:middle"><?=$country->name?></td>
								<td><?=form_dropdown('language_id',$languages,$country->language_id,'class="form-control"')?></td>
								<td style="vertical-align:middle"><?=($country->imported=='no')?form_upload('csv','','class="form-control"'):"Already imported"?></td>
								<td>
								
								<div class="btn-group">
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									Cities <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
									<li><a href="<?=base_url()?>admin/add_city/<?=$country->id?>">Add new city</a></li>
									<li class="divider"></li>
									<?php foreach($country->cities as $city):?>
										<li><a href="<?=base_url()?>admin/edit_city/<?=$city->id?>/<?=$country->id?>"><?=$city->name?></a>
									<?php endforeach;?>
								  </ul>
								</div>
								
								
								<td><?=form_submit('send','Save','class="btn btn-primary"')?></td>
								<td><a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete all data imported for <?=$country->name?>?')" href="<?=base_url()?>admin/delete_cities_from_country_id/<?=$country->id?>">Clear</a>
							</tr>
							<?=form_hidden('country_id',$country->id)?>
						</form>
						<?php endforeach;?> 
						
						</tbody>
					</table>
					
					
				</div>
			</div>
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>