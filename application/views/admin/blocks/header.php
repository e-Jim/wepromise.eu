<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/admin/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?=base_url()?>assets/admin/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/font-awesome/css/font-awesome.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=base_url()?>"><img width="160px" src="<?=base_url();?>assets/frontend/img/logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li class="<?=($this->uri->segment(2)=='countries')?'active':''?>"><a href="<?=base_url()?>admin/countries">Countries</a></li>
            <li class="<?=($this->uri->segment(2)=='languages')?'active':''?>"><a href="<?=base_url()?>admin/languages">Languages</a></li>
            <li class="<?=($this->uri->segment(2)=='candidates')?'active':''?>"><a href="<?=base_url()?>admin/candidates">Candidates</a></li>
			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> CMS <b class="caret"></b></a>
              <ul class="dropdown-menu">
			  <?php $pages = $this->db->get('pages')->result(); ?>
				<?php foreach($pages as $page):?> 
						<li><a href="<?=base_url()?>admin/pages/<?=$page->id?>"><?=$page->general_name?></a></li>
					
				<?php endforeach?>
              </ul>  
            </li> 
			<li class="<?=($this->uri->segment(2)=='translations')?'active':''?>"><a href="<?=base_url()?>admin/translations">Translations</a></li>
			<li class="<?=($this->uri->segment(2)=='support_kits')?'active':''?>"><a href="<?=base_url()?>admin/support_kits">Support kits</a></li>
			<li class="<?=($this->uri->segment(2)=='partners')?'active':''?>"><a href="<?=base_url()?>admin/partners">Partners</a></li>
			<li class="<?=($this->uri->segment(2)=='videos')?'active':''?>"><a href="<?=base_url()?>admin/videos">Videos</a></li>
			<li><a href="<?=base_url()?>admin/get_subscribers" target="_blank">Download subscribers</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li><a href="<?=base_url()?>auth/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
	  