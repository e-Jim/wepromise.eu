<?=$header?>
	<div id="page-wrapper">
		<?php if($this->session->flashdata('errors')||$this->session->flashdata('success')):?>
			<div class="alert alert-dismissable <?=($this->session->flashdata('errors'))?"alert-danger":"alert-success"?>">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('errors')?>
				
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-lg-12">
				<h1>All support kits</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i>All support kits</li>
				</ol>
			</div> 
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
			<!--
				<div class="row">
					<div class="col-lg-3"><input placeholder="Search" name="filter" class="form-control" id="filter-box" value="" maxlength="30" size="30" type="text"></div>
					<div class="col-lg-2"><input id="filter-clear-button" class="btn btn-success"  type="submit" value="Clear"/></div>
				</div>
			-->
				<br/>			
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Language </i></th>
								<th>Upload support kit</th>
								<th>Is uploaded?</th>
								<th>Save</th>
							</tr>
						</thead>
						<tbody>
		  
							<?php foreach($languages as $language):?>
								
									<tr>
									<form method="post" action="" enctype="multipart/form-data" />	
									
										<td style="vertical-align:middle"><?=$language->name?></td>
										<td style="vertical-align:middle"><?=form_upload('support_kit','','class="form-control"')?></td>
										<td><?php if($language->support_kit!="") echo "yes"; else echo "no";?></td>
										<td><?=form_submit('send','Save','class="btn btn-primary"')?></td>
										<?=form_hidden('language_id',$language->id)?>
									</form>	
									</tr>
									
								
								
							<?php endforeach;?>  
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>