<link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap-switch.css" />
<script src="<?=base_url()?>assets/admin/js/bootstrap-switch.js"></script>
<script type="text/javascript">
$('.make-switch').on('switch-change', function (e, data) {
    var value = data.value;
    $('input[name=status]').val(value); 
}); 
</script> 