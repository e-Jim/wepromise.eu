<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.js"></script>
<script type="text/javascript">
	$( document ).ready(function() {
		var country_id,city_id,district_id;
		
/*
		var countries = $('.countries .typeahead').typeahead({
			limit: 10,
			remote: {
			 // url: "<?=base_url()?>admin/get_countries/%QUERY",
			  url: '<?=base_url()?>admin/get_countries/',
					replace: function () {
					
						var q = '<?=base_url()?>admin/get_countries/';
						if ($('.countries .typeahead').val()) {
							q += encodeURIComponent($('.countries .typeahead').val());
						}
						return q;
					
					},
			  filter: function(parsedResponse) {
				var dataset = [];
				for (i = 0; i < parsedResponse.length; i++) {
				  dataset.push({
					value: parsedResponse[i].value,
					tokens: parsedResponse[i].tokens,
					id:parsedResponse[i].id
				  });
				}
				if (parsedResponse.length == 0) {
				  dataset.push({
					value: "No results found" 
				  });
				  $('.cities_suggest').fadeOut();
				 
				  $('.districts_suggest').fadeOut();
				  districts.typeahead('destroy');
				}
				return dataset;
			  }
			}
		});
	
		var cities = $('.cities .typeahead').typeahead(
			{
			
				limit: 10,
				remote: {
				
				 url: '<?=base_url()?>admin/cities/'+country_id+'/',
					replace: function () {
					
						var q1 =  '<?=base_url()?>admin/cities/'+country_id+'/';
						if ($('.countries .typeahead').val()) {
							q1 += encodeURIComponent($('.countries .typeahead').val());
						}
						return q1;
					
					},
				  filter: function(parsedResponse) {
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens,
						id:parsedResponse[i].id
					  });
					}
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No results found" 
					  });
					  console.log('ID-ul tarii il stim oare?'+country_id);
					  $('.districts_suggest').fadeOut();
					  //districts.typeahead('destroy');
					}
					return dataset;
				  }
				}						
			}
		);		 
	
		var districts = $('.districts .typeahead').typeahead(
		{
			limit: 10,
			remote: {
			
			 // url: '<?=base_url()?>admin/get_districts/'+city_id+'/%QUERY',
			  url: '<?=base_url()?>admin/get_districts/'+city_id+'/',

					replace: function () {
					
						var q2 = '<?=base_url()?>admin/get_districts/'+city_id+'/';
						if ($('.countries .typeahead').val()) {
							q2 += encodeURIComponent($('.countries .typeahead').val());
						}
						return q2;
					
					},
			  filter: function(parsedResponse) {
				var dataset = [];
				for (i = 0; i < parsedResponse.length; i++) {
				  dataset.push({
					value: parsedResponse[i].value,
					tokens: parsedResponse[i].tokens,
					id:parsedResponse[i].id
				  });
				}
				if (parsedResponse.length == 0) {
				  dataset.push({
					value: "No results found" 
				  });
				}
				return dataset;
			  }
			}			
		});						
													
*/

		var countries = $('.countries .typeahead').typeahead(
			{
				remote: {
					url: '<?=base_url()?>admin/get_countries/',
					replace: function () {
					
						var q = '<?=base_url()?>admin/get_countries/';
						if ($('.countries .typeahead').val()) {
							q += encodeURIComponent($('.countries .typeahead').val());
						}
						return q;
					
					},
					 filter: function(parsedResponse) {
						var dataset = [];
						for (i = 0; i < parsedResponse.length; i++) {
						  dataset.push({
							value: parsedResponse[i].value,
							tokens: parsedResponse[i].tokens,
							id:parsedResponse[i].id
						  });
						}
						// DACA NU RETURNEAZA NICIO TARA DAM FADE OUT LA ORASE SI LA DISTRICTE SI LE SETAM VALUE GOL
						if (parsedResponse.length == 0) {
						
						  dataset.push({
							value: "No results found" 
						  });
						  
						  $('.cities_suggest').fadeOut();
						  $('.cities .typeahead').typeahead('setQuery', '');

						  $('.districts_suggest').fadeOut();
						  $('.districts .typeahead').typeahead('setQuery', '');

						}
						return dataset;
					  }
				
				}, 

				cache: false,
				limit: 10
			}
		);
		<?php if(set_value('country_id')): $country_id = set_value('country_id'); ?>
			country_id = <?=$country_id?>; 
		<?php endif;?>
		var cities = $('.cities .typeahead').typeahead(
			{
				remote: {
					url: '<?=base_url()?>admin/get_cities/'+country_id+'/',
					replace: function () {
					
						var q2 = '<?=base_url()?>admin/get_cities/'+country_id+'/';
						if ($('.cities .typeahead').val()) {
							q2 += encodeURIComponent($('.cities .typeahead').val());
						}
						return q2;
					
					}, 
				  filter: function(parsedResponse) {
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens,
						id:parsedResponse[i].id
					  });
					}
					// DACA NU RETURNEAZA NICIUN ORAS DAM FADE OUT LA DISTRICTE SI LE SETAM VALUE GOL
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No results found" 
					  });

					  $('.districts_suggest').fadeOut();
					  $('.districts .typeahead').typeahead('setQuery', '');

					}
					return dataset;
				  }
				
				},				

				cache: false,
				limit: 10
			}
		);	
		/*
		var districts = $('.districts .typeahead').typeahead(
			{
				
				remote: {
					url: '<?=base_url()?>admin/get_districts/'+city_id+'/',
					replace: function () {
					
						var q3= '<?=base_url()?>admin/get_districts/'+city_id+'/';
						if ($('.districts .typeahead').val()) {
							q3 += encodeURIComponent($('.districts .typeahead').val());
						}
						return q3;
					
					},
					filter: function(parsedResponse) {
						var dataset = [];
						for (i = 0; i < parsedResponse.length; i++) {
						  dataset.push({
							value: parsedResponse[i].value,
							tokens: parsedResponse[i].tokens,
							id:parsedResponse[i].id
						  });
						}
						if (parsedResponse.length == 0) {
						  dataset.push({
							value: "No results found" 
						  });
						}
						return dataset;
					  } 
					
				}, 

				cache: false,
				limit: 10
			
			//	prefetch: '<?=base_url()?>admin/get_districts/'+city_id+'/',
				//minLength: 0
				 
			}
		);	  
		
		*/
		
		countries.on('typeahead:selected',function(evt,data){

			if(data.value!="No results found"){		
				//cities.typeahead('destroy');			
				country_id = data.id; 
				$('input[name=country_id]').val(country_id);
				console.log(country_id);
				$('.cities_suggest').fadeOut();
				$('.districts_suggest').fadeOut();
				$('.cities .typeahead').typeahead('setQuery', '');
				$('.districts .typeahead').typeahead('setQuery', '');
				$('.cities_suggest').fadeIn();
			} 
		}); 

		cities.on('typeahead:selected',function(evt,data){
			if(data.value!="No results found"){			
				city_id = data.id;
				$('input[name=city_id]').val(city_id);
				$('.districts_suggest').fadeOut();
			//	$('.districts .typeahead').typeahead('setQuery', '');
				$('.districts_dropdown').children('option:not(:first)').remove();
				
				$.ajax({
				  url: '<?=base_url()?>admin/get_districts/'+city_id+'/',
				  cache: false
				})
				  .done(function( html ) {
					$( ".districts_dropdown" ).append( html );
					console.log(html);
				});				
				
				$('.districts_suggest').fadeIn();						
			}
			 
		});		
		
		<?php if(set_value('country_id')): $country_id = set_value('country_id'); ?>	
			$('.countries .typeahead').typeahead('setQuery', '<?=$countries_array[$country_id]?>');
			$('.cities_suggest').fadeIn();
		<?php endif;?> 
		
		<?php if(set_value('city_id')):?>				
		<?php
			$city_id = set_value('city_id');
			if(set_value('district_id')) $url = base_url().'admin/get_districts/'.set_value('city_id').'/'.set_value('district_id');
			else $url = base_url().'admin/get_districts/'.set_value('city_id');
			$city_name = $this->edri->get_city_by_cityID($city_id);
		?>	
			$('.cities .typeahead').typeahead('setQuery', '<?=$city_name?>');
				$.ajax({
				  url: '<?=$url?>',
				  cache: false
				})
				  .done(function( html ) {
					$( ".districts_dropdown" ).append( html );
				});							
			
			$('.districts_suggest').fadeIn();
		<?php endif;?> 		
		
	
		/*
		districts.on('typeahead:selected',function(evt,data){
			if(data.value!="No results found"){			
				district_id = data.id;
			}		
		});	 
		
		
		districts.on('typeahead:opened',function(evt,data){
			$('.districts .typeahead').typeahead('setQuery', 'd'); 
		});
		*/
		
		
		
		$('.cities_suggest .typeahead').change(function() {
			if($(this).val() == ''){
				$('input[name=city_id]').val('');
				$('input[name=district_id]').val('');
				$('.districts_suggest').fadeOut();
			}
		});
  		$('.districts_suggest .typeahead').change(function() {
			if($(this).val() == ''){
				$('input[name=district_id]').val('');
			}
		});    
	}); 	 

</script>

<style type="text/css">
.districts_suggest,.cities_suggest{display:none}



	html {
	  overflow-y: scroll;
	  *overflow-x: hidden;
	}


	.tt-dropdown-menu,
	.gist {
	  text-align: left;
	}




	.example {
	  padding: 0;
	}


	.demo {
	  position: relative;
	  *z-index: 1;

	}
	.twitter-typeahead{
		width:100%
	}
	.typeahead,
	.tt-query,
	.tt-hint,
	select	{
	  width:100%;
	  padding: 8px 12px;
	  font-size: 24px;
	  line-height: 30px;
	  border: 2px solid #ccc;
	  -webkit-border-radius: 8px;
		 -moz-border-radius: 8px;
			  border-radius: 8px;
	  outline: none;
	}

	.typeahead {
	  background-color: #fff;
	}

	.typeahead:focus {
	  border: 2px solid #0097cf;
	}

	.tt-query {
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		 -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
	  color: #999
	}

	.tt-dropdown-menu {
	  width: 100%;
	  margin-top: 12px;
	  padding: 8px 0;
	  background-color: #fff;
	  border: 1px solid #ccc;
	  border: 1px solid rgba(0, 0, 0, 0.2);
	  -webkit-border-radius: 8px;
		 -moz-border-radius: 8px;
			  border-radius: 8px;
	  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		 -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			  box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
	  padding: 3px 20px;
	  font-size: 18px;
	  line-height: 24px;
	}

	.tt-suggestion.tt-is-under-cursor {
	  color: #fff;
	  background-color: #0097cf;

	}

	.tt-suggestion p {
	  margin: 0;
	}

	.gist {
	  font-size: 14px;
	}






	</style>
	<!--
	<style type="text/css">

/* custom font! */
/* ------------ */


/* scaffolding */
/* ----------- */

html {
  overflow-y: scroll;
  *overflow-x: hidden;
}

.container {
  max-width: 700px; 
  margin: 0 auto;
  text-align: center;
}

.tt-dropdown-menu,
.gist {
  text-align: left;
}

/* base styles */
/* ----------- */

html {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
  line-height: 1.2;
  color: #333;
}

.title,
.example-name {

}

p {
  margin: 0 0 10px 0;
}

/* site theme */
/* ---------- */

.title {
  margin: 20px 0 0 0;
  font-size: 64px;
}

.example {
  padding: 30px 0;
}

.example-name {
  margin: 20px 0;
  font-size: 32px;
}

.demo {
  position: relative;
  *z-index: 1;
  margin: 50px 0;
}

.typeahead,
.tt-query,
.tt-hint {
  width: 396px;

  padding: 8px 12px;
  font-size: 24px;
  line-height: 30px;
  border: 2px solid #ccc;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  outline: none;
}

.typeahead {
  background-color: #fff;
}

.typeahead:focus {
  border: 2px solid #0097cf;
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-dropdown-menu {
  width: 422px;
  margin-top: 12px;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion.tt-is-under-cursor {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}

.gist {
  font-size: 14px;
}

/* example specific styles */
/* ----------------------- */

.example-twitter-oss .tt-suggestion {
  padding: 8px 20px;
}

.example-twitter-oss .tt-suggestion + .tt-suggestion {
  border-top: 1px solid #ccc;
}

.example-twitter-oss .repo-language {
  float: right;
  font-style: italic;
}

.example-twitter-oss .repo-name {
  font-weight: bold;
}

.example-twitter-oss .repo-description {
  font-size: 14px;
}

.example-sports .league-name {
  margin: 0 20px 5px 20px;
  padding: 3px 0;
  border-bottom: 1px solid #ccc;
}

.example-arabic .tt-dropdown-menu {
  text-align: right;
}

	</style>
	-->