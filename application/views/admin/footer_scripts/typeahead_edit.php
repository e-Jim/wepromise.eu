<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.js"></script>
<?php 
	$country_id  = (set_value('country_id'))?set_value('country_id'):$candidate->country_id; 
	$city_id 	 = (set_value('city_id'))?set_value('city_id'):$candidate->city_id; 
	$district_id = (set_value('district_id'))?set_value('district_id'):$candidate->district_id;
?>
<script type="text/javascript">
	$( document ).ready(function() {
	
		var country_id,city_id,district_id;
		
		var countries = $('.countries .typeahead').typeahead(
			{
				remote: {
					url: '<?=base_url()?>admin/get_countries/',
					replace: function () {
					
						var q = '<?=base_url()?>admin/get_countries/';
						if ($('.countries .typeahead').val()) {
							q += encodeURIComponent($('.countries .typeahead').val());
						}
						return q;
					
					},
					 filter: function(parsedResponse) {
						var dataset = [];
						for (i = 0; i < parsedResponse.length; i++) {
						  dataset.push({
							value: parsedResponse[i].value,
							tokens: parsedResponse[i].tokens,
							id:parsedResponse[i].id
						  });
						}
						// DACA NU RETURNEAZA NICIO TARA DAM FADE OUT LA ORASE SI LA DISTRICTE SI LE SETAM VALUE GOL
						if (parsedResponse.length == 0) {
						
						  dataset.push({
							value: "No results found" 
						  });
						  
						  $('.cities_suggest').fadeOut();
						  $('.cities .typeahead').typeahead('setQuery', '');

						  $('.districts_suggest').fadeOut();
						  $('.districts .typeahead').typeahead('setQuery', '');

						}
						return dataset;
					  }
				
				}, 

				cache: false,
				limit: 10
			}
		);
		
		countries.on('typeahead:selected',function(evt,data){

			if(data.value!="No results found"){					
				country_id = data.id; 
				$('input[name=country_id]').val(country_id);
				<?php if($city_id!=0):?>
				$('.cities_suggest').fadeOut();
				$('.districts_suggest').fadeOut();
				$('.cities .typeahead').typeahead('setQuery', '');
				$('.districts .typeahead').typeahead('setQuery', '');
				$('.cities_suggest').fadeIn();
				<?php endif;?>
			} 
		}); 
		
		
		
		
		country_id = <?=$country_id?>; 
		
		var cities = $('.cities .typeahead').typeahead(
			{
				remote: {
					url: '<?=base_url()?>admin/get_cities/'+country_id+'/',
					replace: function () {
					
						var q2 = '<?=base_url()?>admin/get_cities/'+country_id+'/';
						if ($('.cities .typeahead').val()) {
							q2 += encodeURIComponent($('.cities .typeahead').val());
						}
						return q2;
					
					}, 
				  filter: function(parsedResponse) {
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens,
						id:parsedResponse[i].id
					  });
					}
					// DACA NU RETURNEAZA NICIUN ORAS DAM FADE OUT LA DISTRICTE SI LE SETAM VALUE GOL
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No results found" 
					  });
					<?php if($city_id==0):?>
					  $('.districts_suggest').fadeOut();
					  $('.districts .typeahead').typeahead('setQuery', '');
					<?php endif;?>
					}
					return dataset;
				  }
				
				},				

				cache: false,
				limit: 10
			}
		);	
	
		


		cities.on('typeahead:selected',function(evt,data){
			if(data.value!="No results found"){			
				city_id = data.id;
				$('input[name=city_id]').val(city_id);
				$('.districts_suggest').fadeOut();
			//	$('.districts .typeahead').typeahead('setQuery', '');
				$('.districts_dropdown').children('option:not(:first)').remove();
				<?php //if($city_id!=0):?>
				$.ajax({
				  url: '<?=base_url()?>admin/get_districts/'+city_id+'/',
				  cache: false
				})
				  .done(function( html ) {
					$( ".districts_dropdown" ).append( html );
					console.log(html);
				});				
				
				$('.districts_suggest').fadeIn();	
				<?php //endif;?>
			}
			 
		});		
		
		<?php //$country_id = (set_value('country_id'))?set_value('country_id'):$candidate->country_id; ?>	
			$('.countries .typeahead').typeahead('setQuery', '<?=$countries_array[$country_id]?>');
			$('.cities_suggest').fadeIn();
		
		
				
		<?php
			if($city_id!=0&&$district_id!=0)
			$url = base_url().'admin/get_districts/'.$city_id.'/'.$district_id;
			if($city_id!=0&&$district_id==0)
			$url = base_url().'admin/get_districts/'.$city_id;
		?>	
		<?php if($city_id!=0):
			$city_name = $this->edri->get_city_by_cityID($city_id);
		
		?>
			$('.cities .typeahead').typeahead('setQuery', '<?=$city_name?>');
				$.ajax({
				  url: '<?=$url?>',
				  cache: false 
				})
				  .done(function( html ) {
					$( ".districts_dropdown" ).append( html );
				});							
			
			$('.districts_suggest').fadeIn();
		<?php endif;?>
		$('.cities_suggest .typeahead').change(function() {
			if($(this).val() == ''){
				$('input[name=city_id]').val('');
				$('select[name=district_id]').val('');
				$('.districts_suggest').fadeOut();
			}
		});
  		$('.districts_suggest .typeahead').change(function() {
			if($(this).val() == ''){
				$('select[name=district_id]').val('');
			}
		}); 
	}); 	 

</script>
