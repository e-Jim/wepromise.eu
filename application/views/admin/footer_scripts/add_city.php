<script type="text/javascript">
$(document).ready(function(){
	
    var next = parseInt($("#count").val());
	$(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#row" + next;
        next = next + 1;
   
		
		var newIn = '<br/><div class="row" id="row'+next+'"><div class="col-sm-3"><input autocomplete="off" class="form-control" id="district' + next + '" name="district' + next + '" type="text" /></div><div class="col-sm-7"><input autocomplete="off" class="form-control" id="zipcodes' + next + '" name="zipcodes' + next + '" type="text" /></div><div class="col-sm-2"><label class="control-label"><a class="remove-row">Remove</a></label></div></div>';
					
        var newInput = $(newIn);
        $(addto).after(newInput);
        $("#count").val(next);  
    });
	
	$(".row").on('click','.remove-row',function(){
		$(this).parent().parent().parent().remove();
		 next = next - 1;
		 $("#count").val(next);  
	});
});  
</script>  