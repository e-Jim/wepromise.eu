<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/wysihtml5/src/bootstrap-wysihtml5.css" />
<script src="<?=base_url()?>assets/admin/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
<script src="<?=base_url()?>assets/admin/wysihtml5/src/bootstrap3-wysihtml5.js"></script>
<?php if(isset($candidate)&&$candidate):?>
<script>
    $('textarea').wysihtml5({ "image": false, "font-styles": false,"lists": false});
</script> 
<?php 
else: 
?>
<script>
    $('textarea').wysihtml5({ "image": false,"html": true});
</script> 
<?php endif;?>