<script src="<?=base_url()?>assets/admin/js/tablesorter/jquery.tablesorter.js"></script>
<script src="<?=base_url()?>assets/admin/js/tablesorter/tablesorter_filter.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".tablesorter")
	.tablesorter({debug: false})
	.tablesorterFilter({filterContainer: "#filter-box",
						filterClearContainer: "#filter-clear-button",
						filterColumns: [0]})

});

</script>