<?=$header?>
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=validation_errors()?>
				
			</div>
		<?php endif;?>
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-sm-12">
				<h1>Edit candidate</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li><a href="<?=base_url()?>admin/candidates">Candidates</a></li>
				<li class="active"><i class="icon-file-alt"></i>Edit candidate</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" enctype="multipart/form-data" >
					<div class="row">
						<div class="col-sm-4">
							<?php $name_val = (set_value('name'))?set_value('name'):$candidate->name; ?>
							<?=form_label('Name','','class="form-control"')?><br/>
							<?=form_input('name',$name_val,'class="form-control"')?>
						</div>
						<div class="col-sm-4">
							<?php $website_val = (set_value('website'))?set_value('website'):$candidate->website; ?>
							<?=form_label('Website (Ex:http://www.website.com)','','class="form-control"')?> <br/>
							<?=form_input('website',$website_val,'class="form-control"')?> 
						</div>	
						<div class="col-sm-4">
							<?php $party_name = (set_value('party_name'))?set_value('party_name'):$candidate->party_name; ?>
							<?=form_label('Party name','','class="form-control"')?> <br/>
							<?=form_input('party_name',$party_name,'class="form-control"')?> 
						</div>	
						
					</div>
					
					<br/>	
					<div class="row">
						<div class="col-sm-3">			
							<?php $facebook = (set_value('facebook'))?set_value('facebook'):$candidate->facebook; ?>						
							<?=form_label('Facebook','','class="form-control"')?><br/>
							<?=form_input('facebook',$facebook,'class="form-control"')?>
						</div>
						<div class="col-sm-3">
							<?php $twitter = (set_value('twitter'))?set_value('twitter'):$candidate->twitter; ?>
							<?=form_label('Twitter (Ex:http://www.twitter.com/username)','','class="form-control"')?> <br/>
							<?=form_input('twitter',$twitter,'class="form-control"')?> 
						</div>	
						<div class="col-sm-3">
							<?php $gplus = (set_value('gplus'))?set_value('gplus'):$candidate->gplus; ?>
							<?=form_label('Google+','','class="form-control"')?> <br/>
							<?=form_input('gplus',$gplus,'class="form-control"')?> 
						</div>							
						<div class="col-sm-3">
							<?php $youtube = (set_value('youtube'))?set_value('youtube'):$candidate->youtube; ?>
							<?=form_label('Youtube','','class="form-control"')?> <br/>
							<?=form_input('youtube',$youtube,'class="form-control"')?> 
						</div>											
					</div>
					<br/>							
					<div class="row">
		
						<div class="col-sm-12 candidate_description">
							<label>Candidate description (15 to 75 words)</label><br/>
							<ul class="nav nav-tabs">
							<?php $i=0;  foreach($languages as $language):?>
								<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
									<a data-toggle="tab" href="#<?=strtolower($language->name)?>">
										<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
									</a>
								</li>
							<?php $i++; endforeach;?>
							
							</ul> 

							<!-- Tab panes -->
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>">
									<br/>
									<?php $vl = 'desc_'.$language->id; $desc_val = (set_value('desc_'.$language->id))?set_value('desc_'.$language->id):$candidate->$vl; ?>
									<textarea class="form-control" cols="4" style="height:300px" name="desc_<?=$language->id?>"><?=$desc_val?></textarea>
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					
					</div> 
					<br/>  
					<div class="row">
						<div class="col-sm-4">		
							<label>Change current country (<?=$countries_array[$candidate->country_id]?>)</label><br/>
  
							<div class="example countries"> 
								<div class="demo">
								  <input class="typeahead" type="text" name="typeahead" placeholder="Country">
								  <input type="hidden" name="country_id" value="<?=(set_value('country_id'))?set_value('country_id'):$candidate->country_id?>" >
								</div>
							</div>						
						</div>
						<div class="col-sm-4">		 
							<div class="cities_suggest">
								<label>Change current city (<?=$this->edri->get_city_by_cityID($candidate->city_id);?>)</label><br/>
	  
								<div class="example cities"> 
									<div class="demo">
									  <input class="typeahead" type="text" name="typeahead" placeholder="City">
									  <input type="hidden" name="city_id" value="<?=(set_value('city_id'))?set_value('city_id'):$candidate->city_id?>" >
									</div>
								</div>		
							</div>
						</div>
						<div class="col-sm-4">		
							<div class="districts_suggest">
							
								<label>Change current district (<?=$this->edri->get_district_by_districtID($candidate->district_id)?>)</label><br/>
								<div class="example districts"> 
									<div class="demo">
									  <!--<input class="typeahead" type="text" name="typeahead" placeholder="Districts">-->
									  <select name="district_id" class="districts_dropdown">
										<option>Please select district</option>
									  </select>
									</div>
								</div>	
								
							</div>
						</div>						
					</div>
					<br/>	
					<div class="row">
						<div class="col-sm-6">
							<label>Candidate profile picture</label>
							<input type="file"  name="photo">
							<?php if($candidate->image):?>
							<img src="<?=base_url()?>uploads/candidates/<?=$candidate->image?>">
							<?php endif;?> 
						</div>
						<div class="col-sm-3">
							<?php $statuses=array('activ'=>'Activ','inactiv'=>'Inactiv');?>
							<label>Status</label>
							<?php $the_status = (set_value('status'))?set_value('status'):$candidate->status; ?>
							<?php echo form_dropdown('status',$statuses,$the_status);?>
						</div>
						<div class="col-sm-3">
							
							<label>Winner?</label>
							<?php $the_status2 = (set_value('winner'))?set_value('winner'):$candidate->winner; ?>
							<?php echo form_dropdown('winner',$statuses,$the_status2);?>
						</div>						
					</div>
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<input class="btn btn-primary" type="submit" name="send" value="Update candidate">
						</div>
					</div>
				</form>
					 

		 
			</div>  
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>