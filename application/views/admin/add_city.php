<?=$header?> 
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=validation_errors()?>
				
			</div> 
		<?php endif;?>
		<div class="row">
			<div class="col-sm-12">
				<h1>Add city</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li><a href="<?=base_url()?>admin/countries">Countries</a></li>
				<li class="active"><i class="icon-file-alt"></i> Add city</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" enctype="multipart/form-data" >
					<div class="row">
						<div class="col-sm-3">					
							<?=form_label('City name','','class="form-control"')?><br/>
							<?=form_input('city_name',set_value('city_name'),'class="form-control"')?>
						</div>								
					</div> 
					<br/> 
					<?php if($this->input->post()):?>
						<?php for($i=1;$i<=$this->input->post('count');$i++):?>
							<?php if($i==1):?>
								<div class="row" id="row1">
									<div class="col-sm-3">					
										<label class="control-label" for="district1">Districts <a class="add-more" >+ Add new district</a></label>		
										<input value="<?=set_value('district1')?>" autocomplete="off" class="form-control" id="district1" name="district1" type="text" />							
									</div>	
									<div class="col-sm-9">					
										<label class="control-label" for="zipcodes1">ZIP Codes (EX: 231range322,344,544,653)</label>									
										<input value="<?=set_value('zipcodes1')?>"  autocomplete="off" class="form-control" id="zipcodes1" name="zipcodes1" type="text" />
									</div>										
								</div>	
							<?php else:?>
								<br/>
								<div class="row" id="row<?=$i?>">
									<div class="col-sm-3">					
										<input value="<?=set_value('district'.$i)?>" autocomplete="off" class="form-control" id="district<?=$i?>" name="district<?=$i?>" type="text" />
									</div>	
									<div class="col-sm-8">						
										<input value="<?=set_value('zipcodes'.$i)?>"  autocomplete="off" class="form-control" id="zipcodes<?=$i?>" name="zipcodes<?=$i?>" type="text" />
									</div>	
									<div class="col-sm-1">
									<label class="control-label"><a class="remove-row">- Remove</a></label>
									</div>
								</div>							 
							<?php endif;?>
						<?php endfor;?>
					<?php else:?>
						<div class="row" id="row1">
							<div class="col-sm-3">					
								<label class="control-label" for="district1">Districts <a class="add-more" >+ Add new district</a></label>		
								<input autocomplete="off" class="form-control" id="district1" name="district1" type="text" />							
							</div>	
							<div class="col-sm-9">					
								<label class="control-label" for="zipcodes1">ZIP Codes (EX: 231range322,344,544,653)</label>									
								<input autocomplete="off" class="form-control" id="zipcodes1" name="zipcodes1" type="text" />									
							</div>										
						</div>	
					<?php endif;?>			 		
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<input type="hidden" name="count" value="<?=(set_value('count'))?set_value('count'):'1'?>" id="count" />
							<input class="btn btn-primary" type="submit" name="send" value="Add city">
						</div>
					</div>
					
				</form>
					

		 
			</div>  
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>