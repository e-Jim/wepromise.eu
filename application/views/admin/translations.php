<?=$header?>
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=validation_errors()?>
				
			</div>
		<?php endif;?>
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-sm-12">
				<h1>Translations</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i>Translations</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" >
					
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: hero-panel title</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>hero_title">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>hero_title">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['hero_title']))?$translation[0]['hero_title']:"";
										$hero_title = (set_value('hero_title'.$language->id))?set_value('hero_title'.$language->id):$translation;
									?> 
									
									
								
									<input type="text" class="form-control" name="hero_title<?=$language->id?>" value="<?=$hero_title?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
						
					</div> 	
					
					<br/>

					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: hero-panel subtitle</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>hero_subtitle">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>hero_subtitle">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['hero_subtitle']))?$translation[0]['hero_subtitle']:"";
										$hero_subtitle = (set_value('hero_subtitle'.$language->id))?set_value('hero_subtitle'.$language->id):$translation;
									?> 
									
									
								
									<input type="text" class="form-control" name="hero_subtitle<?=$language->id?>" value="<?=$hero_subtitle?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
						
					</div> 	
					
					<br/>	
					
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage AND Search results: Input `SEARCH` placeholder</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>search_placeholder">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>search_placeholder">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['search_placeholder']))?$translation[0]['search_placeholder']:"";
										$search_placeholder = (set_value('search_placeholder'.$language->id))?set_value('search_placeholder'.$language->id):$translation;
									?> 
									
									 
								
									<input type="text" class="form-control" name="search_placeholder<?=$language->id?>" value="<?=$search_placeholder?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
						
					</div> 	 
					
					<br/>	
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage AND Search results: `Promise to vote for any candidate who supports the` </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>promise_to_vote_intro">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>promise_to_vote_intro">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['promise_to_vote_intro']))?$translation[0]['promise_to_vote_intro']:"";
										$promise_to_vote_intro = (set_value('promise_to_vote_intro'.$language->id))?set_value('promise_to_vote_intro'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="promise_to_vote_intro<?=$language->id?>" value="<?=$promise_to_vote_intro?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage AND Search results: `10 point Charter of` </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>point_charter">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>point_charter">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['point_charter']))?$translation[0]['point_charter']:"";
										$point_charter = (set_value('point_charter'.$language->id))?set_value('point_charter'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="point_charter<?=$language->id?>" value="<?=$point_charter?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 	
					
					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage AND Search results: `Digital Rights.` </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>digital_rights">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>digital_rights">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['digital_rights']))?$translation[0]['digital_rights']:"";
										$digital_rights = (set_value('digital_rights'.$language->id))?set_value('digital_rights'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="digital_rights<?=$language->id?>" value="<?=$digital_rights?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 			
					
					<br/>	
					
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage AND Search results: Input `NAME` placeholder</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>name_placeholder">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>name_placeholder">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['name_placeholder']))?$translation[0]['name_placeholder']:"";
										$name_placeholder = (set_value('name_placeholder'.$language->id))?set_value('name_placeholder'.$language->id):$translation;
									?> 
									<input type="text" class="form-control" name="name_placeholder<?=$language->id?>" value="<?=$name_placeholder?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>
						
					</div> 	 
					
					<br/>	
					
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage AND Search results: Input `COUNTRY` placeholder</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>country_placeholder">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>country_placeholder">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['country_placeholder']))?$translation[0]['country_placeholder']:"";
										$country_placeholder = (set_value('country_placeholder'.$language->id))?set_value('country_placeholder'.$language->id):$translation;
									?> 
									<input type="text" class="form-control" name="country_placeholder<?=$language->id?>" value="<?=$country_placeholder?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>
						
					</div> 	 
					
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage AND Search results: Input Submit `Promise to vote` (text button)</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>promise_to_vote">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>promise_to_vote">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['promise_to_vote']))?$translation[0]['promise_to_vote']:"";
										$promise_to_vote = (set_value('promise_to_vote'.$language->id))?set_value('promise_to_vote'.$language->id):$translation;
									?> 
									<input type="text" class="form-control" name="promise_to_vote<?=$language->id?>" value="<?=$promise_to_vote?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>
						
					</div> 	
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage AND Search results: Input Submit *(number automatically completed) `fellow citizens promised to vote`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>promised_to_vote">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>promised_to_vote">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['promised_to_vote']))?$translation[0]['promised_to_vote']:"";
										$promised_to_vote = (set_value('promised_to_vote'.$language->id))?set_value('promised_to_vote'.$language->id):$translation;
									?> 
									<input type="text" class="form-control" name="promised_to_vote<?=$language->id?>" value="<?=$promised_to_vote?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
						
					</div> 					 
					
					<br/>					
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: Left Block Title `Your election promise`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>election_promise_title">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>election_promise_title">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['election_promise_title']))?$translation[0]['election_promise_title']:"";
										$election_promise_title = (set_value('election_promise_title'.$language->id))?set_value('election_promise_title'.$language->id):$translation;
									?>  
									<input type="text" class="form-control" name="election_promise_title<?=$language->id?>" value="<?=$election_promise_title?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>
						
					</div> 	
					
					<br/>	

					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: Left Block Content `Your election promise content`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>election_promise_content">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>election_promise_content">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['election_promise_content']))?$translation[0]['election_promise_content']:"";
										$election_promise_content = (set_value('election_promise_content'.$language->id))?set_value('election_promise_content'.$language->id):$translation;
									?> 
									<textarea style="height:300px" class="form-control" cols="4" name="election_promise_content<?=$language->id?>"><?=$election_promise_content?></textarea>
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 
					
					<br/>	

					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Center Block Title `Their election promise`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>their_election_promise">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>their_election_promise">
									<br/>
									<?php
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['their_election_promise']))?$translation[0]['their_election_promise']:"";
										$their_election_promise = (set_value('their_election_promise'.$language->id))?set_value('their_election_promise'.$language->id):$translation;
									?> 
									<input type="text" class="form-control" name="their_election_promise<?=$language->id?>" value="<?=$their_election_promise?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 
					
					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Center Block Content `Their election promise content`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>their_election_promise_content">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>their_election_promise_content">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['their_election_promise_content']))?$translation[0]['their_election_promise_content']:"";
										$their_election_promise_content = (set_value('their_election_promise_content'.$language->id))?set_value('their_election_promise_content'.$language->id):$translation; 	  
									?> 
									<textarea class="form-control" style="height:300px" cols="4" name="their_election_promise_content<?=$language->id?>"><?=$their_election_promise_content?></textarea>
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 	 

					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Center Block Button `Their election promise button`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>their_election_promise_button">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>their_election_promise_button">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['their_election_promise_button']))?$translation[0]['their_election_promise_button']:"";
										$their_election_promise_button = (set_value('their_election_promise_button'.$language->id))?set_value('their_election_promise_button'.$language->id):$translation;	  
									?> 
									<input type="text" class="form-control"  name="their_election_promise_button<?=$language->id?>" value="<?=$their_election_promise_button?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 	
					
					<br/>	
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Center Block Button `Sign the charter`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>sign_the_charter">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>sign_the_charter">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['sign_the_charter']))?$translation[0]['sign_the_charter']:"";
										$sign_the_charter = (set_value('sign_the_charter'.$language->id))?set_value('sign_the_charter'.$language->id):$translation;	  
									?> 
									<input type="text" class="form-control"  name="sign_the_charter<?=$language->id?>" value="<?=$sign_the_charter?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 	
					
					<br/>
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Right Block Title `What can I do?`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>what_can_i_do">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>what_can_i_do">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['what_can_i_do']))?$translation[0]['what_can_i_do']:"";
										$what_can_i_do = (set_value('what_can_i_do'.$language->id))?set_value('what_can_i_do'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="what_can_i_do<?=$language->id?>" value="<?=$what_can_i_do?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: Right Block Content `What can I do?` </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>what_can_i_do_content">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>what_can_i_do_content">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['what_can_i_do_content']))?$translation[0]['what_can_i_do_content']:"";
										$what_can_i_do_content = (set_value('what_can_i_do_content'.$language->id))?set_value('what_can_i_do_content'.$language->id):$translation;	  
									?> 
									<textarea class="form-control" style="height:300px" name="what_can_i_do_content<?=$language->id?>"><?=$what_can_i_do_content?></textarea>
								</div>
							<?php $i++; endforeach;?>  
							</div>
						</div>				
					</div>
					
					<br/>	

					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Show your representatives that you care`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>you_care">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>you_care">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['you_care']))?$translation[0]['you_care']:"";
										$you_care = (set_value('you_care'.$language->id))?set_value('you_care'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="you_care<?=$language->id?>" value="<?=$you_care?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>

					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Read the 10 principles`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>read_principles">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>read_principles">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['read_principles']))?$translation[0]['read_principles']:"";
										$read_principles = (set_value('read_principles'.$language->id))?set_value('read_principles'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="read_principles<?=$language->id?>" value="<?=$read_principles?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Signups map`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>signups_map">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>signups_map">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['signups_map']))?$translation[0]['signups_map']:"";
										$signups_map = (set_value('signups_map'.$language->id))?set_value('signups_map'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="signups_map<?=$language->id?>" value="<?=$signups_map?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 				
					
					<br/>	
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Signups map` (description)</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>signups_map_desc">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>signups_map_desc">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['signups_map_desc']))?$translation[0]['signups_map_desc']:"";
										$signups_map_desc = (set_value('signups_map_desc'.$language->id))?set_value('signups_map_desc'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="signups_map_desc<?=$language->id?>" value="<?=$signups_map_desc?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>	
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Recent candidates`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>recent_candidates">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>recent_candidates">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['recent_candidates']))?$translation[0]['recent_candidates']:"";
										$recent_candidates = (set_value('recent_candidates'.$language->id))?set_value('recent_candidates'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="recent_candidates<?=$language->id?>" value="<?=$recent_candidates?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					<br/>	
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Candidates` (from bottom map)</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>candidates">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>candidates">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['candidates']))?$translation[0]['candidates']:"";
										$candidates = (set_value('candidates'.$language->id))?set_value('candidates'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="candidates<?=$language->id?>" value="<?=$candidates?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 					
					<br/>						
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Spread the news`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>spread_news">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>spread_news">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['spread_news']))?$translation[0]['spread_news']:"";
										$spread_news = (set_value('spread_news'.$language->id))?set_value('spread_news'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="spread_news<?=$language->id?>" value="<?=$spread_news?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>

					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `Download support kit`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>download_kit">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>download_kit">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['download_kit']))?$translation[0]['download_kit']:"";
										$download_kit = (set_value('download_kit'.$language->id))?set_value('download_kit'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="download_kit<?=$language->id?>" value="<?=$download_kit?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>
					
					<div class="row"> 
						<div class="col-sm-12">
							<label>Homepage: `CC (2013). This website is published under a creative commons licence.`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>footer_copyright">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>footer_copyright">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['footer_copyright']))?$translation[0]['footer_copyright']:"";
										$footer_copyright = (set_value('footer_copyright'.$language->id))?set_value('footer_copyright'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="footer_copyright<?=$language->id?>" value="<?=$footer_copyright?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>	
					<div class="row"> 
						<div class="col-sm-12">
							<label>Results page: *(number generated automatically) `Results for` *(search term generated automatically)</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>results_for">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>results_for">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['results_for']))?$translation[0]['results_for']:"";
										$results_for = (set_value('results_for'.$language->id))?set_value('results_for'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="results_for<?=$language->id?>" value="<?=$results_for?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>		

					<div class="row"> 
						<div class="col-sm-12">
							<label>Results page: `Results section` *(number generated automatically)</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>results_section">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								 
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>results_section">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['results_section']))?$translation[0]['results_section']:"";
										$results_section = (set_value('results_section'.$language->id))?set_value('results_section'.$language->id):$translation; 
									?> 
									<input type="text" class="form-control" name="results_section<?=$language->id?>" value="<?=$results_section?>">
								</div>
							<?php $i++; endforeach;?> 
							</div>
						</div>				
					</div> 		
					
					<br/>	

					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: Meta title</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_title">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_title">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['meta_title']))?$translation[0]['meta_title']:"";
										$meta_title = (set_value('meta_title'.$language->id))?set_value('meta_title'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="meta_title<?=$language->id?>" value="<?=$meta_title?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 						
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: Meta description</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_description">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_description">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['meta_description']))?$translation[0]['meta_description']:"";
										$meta_description = (set_value('meta_description'.$language->id))?set_value('meta_description'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="meta_description<?=$language->id?>" value="<?=$meta_description?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Homepage: Meta keywords</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_keywords">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_keywords">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['meta_keywords']))?$translation[0]['meta_keywords']:"";
										$meta_keywords = (set_value('meta_keywords'.$language->id))?set_value('meta_keywords'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="meta_keywords<?=$language->id?>" value="<?=$meta_keywords?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>

					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `Success message` </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>success_message">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>success_message">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['success_message']))?$translation[0]['success_message']:"";
										$success_message = (set_value('success_message'.$language->id))?set_value('success_message'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="success_message<?=$language->id?>" value="<?=$success_message?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>
					<?php /*
					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `All fields required` error</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>fields_required">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>fields_required">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['fields_required']))?$translation[0]['fields_required']:"";
										$fields_required = (set_value('fields_required'.$language->id))?set_value('fields_required'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="fields_required<?=$language->id?>" value="<?=$fields_required?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					
					*/ ?>
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `Duplicate email` error </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>duplicate_email">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>duplicate_email">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['duplicate_email']))?$translation[0]['duplicate_email']:"";
										$duplicate_email = (set_value('duplicate_email'.$language->id))?set_value('duplicate_email'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="duplicate_email<?=$language->id?>" value="<?=$duplicate_email?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `Please validate email` message </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>please_validate_email">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>please_validate_email">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['please_validate_email']))?$translation[0]['please_validate_email']:"";
										$please_validate_email = (set_value('please_validate_email'.$language->id))?set_value('please_validate_email'.$language->id):$translation;  
									?> 
									
								 		
								 
									<input type="text" class="form-control" cols="4" name="please_validate_email<?=$language->id?>" value="<?=$please_validate_email?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					  
					</div> 	
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `Validation error` message </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>validation_error">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>validation_error">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['validation_error']))?$translation[0]['validation_error']:"";
										$validation_error = (set_value('validation_error'.$language->id))?set_value('validation_error'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="validation_error<?=$language->id?>" value="<?=$validation_error?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Promise to vote form: `Recieve news` checkbox message </label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>receive_newsletter">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>receive_newsletter">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['receive_newsletter']))?$translation[0]['receive_newsletter']:"";
										$receive_newsletter = (set_value('receive_newsletter'.$language->id))?set_value('receive_newsletter'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="receive_newsletter<?=$language->id?>" value="<?=$receive_newsletter?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 		
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>CMS pages: `Text below search input`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>text_below_search_cms">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>text_below_search_cms">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['text_below_search_cms']))?$translation[0]['text_below_search_cms']:"";
										$text_below_search_cms = (set_value('text_below_search_cms'.$language->id))?set_value('text_below_search_cms'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="text_below_search_cms<?=$language->id?>" value="<?=$text_below_search_cms?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 		
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<label>Form Error: `Name field is required.`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>name_required">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>name_required">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['name_required']))?$translation[0]['name_required']:"";
										$name_required = (set_value('name_required'.$language->id))?set_value('name_required'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="name_required<?=$language->id?>" value="<?=$name_required?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 						
					<br/>		
					<div class="row">
						<div class="col-sm-12">
							<label>Form Error: `Email field is required.`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>email_required">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>email_required">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['email_required']))?$translation[0]['email_required']:"";
										$email_required = (set_value('email_required'.$language->id))?set_value('email_required'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="email_required<?=$language->id?>" value="<?=$email_required?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>		 
					</div> 	
					<br/>		
					<div class="row">
						<div class="col-sm-12">
							<label>Form Error: `Country field is required.`</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>country_required">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li> 
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>country_required">
									<br/>
									<?php 
										$translation = $this->edri->search_array($translations,'language_id',$language->id);
										$translation = (isset($translation[0]['country_required']))?$translation[0]['country_required']:"";
										$country_required = (set_value('country_required'.$language->id))?set_value('country_required'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="country_required<?=$language->id?>" value="<?=$country_required?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>		 
					</div> 						
					<br/>					
					<div class="row">
						<div class="col-sm-12">
							<input class="btn btn-primary" type="submit" name="send" value="Update page">
						</div>
					</div>
				</form>
					 
			</div>  
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>