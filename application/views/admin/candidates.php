<?=$header?>
	<div id="page-wrapper">
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-lg-12">
				<h1>Candidates</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i>Candidates</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-3"><input placeholder="Search" name="filter" class="form-control" id="filter-box" value="" maxlength="30" size="30" type="text"></div>
					<div class="col-lg-1"><input id="filter-clear-button" class="btn btn-success"  type="submit" value="Clear"/></div>
					<div class="col-lg-3">
						<a class="btn btn-success" href="<?=base_url()?>admin/add_candidate">Add candidate</a>
					</div>
					<div class="col-lg-5">
						<div id="pager" class="pager pull-right">
							<form>
								<i class="fa fa-angle-double-left first"></i> 
								<i class="fa fa-angle-left prev"></i> 
								<input type="text" class="pagedisplay form-control">
								<i class="fa fa-angle-right next"></i> 
								<i class="fa fa-angle-double-right last"></i> 
								<select class="pagesize form-control">
									<option selected="selected" value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
								</select>
							</form>
						</div>
					</div>				
				</div>
				<br/>
				<div class="table-responsive">
					<table class="table table-bordered table-hover tablesorter">
						<thead>
							<tr>
								<th>Name <i class="fa fa-sort"></i></th>
								<th>Country <i class="fa fa-sort"></i></th>
								<th>City <i class="fa fa-sort"></i></th>
								<th>District <i class="fa fa-sort"></i></th>
								<th>Edit <i class="fa fa-sort"></i></th>
								<th>Delete <i class="fa fa-sort"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($candidates as $candidate):?>
						
							<tr>
								<td style="vertical-align:middle"><?=$candidate->name?></td>
								<td style="vertical-align:middle"><?=$candidate->country?></td>
								<td style="vertical-align:middle"><?=$candidate->city?></td>
								<td style="vertical-align:middle"><?=$candidate->district?></td>
								<td style="vertical-align:middle"><a href="<?=base_url()?>admin/candidate/<?=$candidate->id?>" class="btn btn-primary">Edit</a></td>
								<td style="vertical-align:middle"><a onclick="return confirm('Do you want to delete <?=$candidate->name?>?')" href="<?=base_url()?>admin/delete_candidate/<?=$candidate->id?>" class="btn btn-danger">Delete</a></td>
								
							</tr>
							
						<?php endforeach;?>
						
						</tbody> 
					</table>
					
					 
				</div>
			</div>
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>