<?=$header?> 
	<div id="page-wrapper">
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>		
		 
		<div class="row">
			<div class="col-sm-12">
				<h1>Partners</h1>
				<ol class="breadcrumb">
					<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
					<li class="active">Partners</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" enctype="multipart/form-data" >
				<div class="row">
				
					<div class="col-sm-6">
						<label>Name</label>
						<input type="text" class="form-control" name="name">
					</div>

					<div class="col-sm-6">
						<label>Link</label>
						<input type="text"  class="form-control" name="link">
					</div>
				</div>	
				<br/>				
				<div class="row">
					<div class="col-sm-12">
						<label>Logo</label>
						<input type="file" name="partners">
					</div>
				</div>
				<br/>
				<div class="row">
					
					<div class="col-sm-12">
						<input class="btn btn-primary" type="submit" name="send" value="Add partner">
					</div>
				</div>
				</form>
			</div>  
		</div>
		<br/>
		<div class="row">
		<?php 
		
			foreach($partners as $partner):		
		?>
		<div class="col-sm-2"> 
			<form method="post" action="">
				<img class="img-responsive" src="<?=base_url()?>/uploads/partners/<?=$partner->image?>"><br/>
				<label>Name</label>
				<input type="text" class="form-control" value="<?=$partner->name?>" name="name"><br/>
				<label>Link</label>
				<input type="text"  class="form-control" value="<?=$partner->link?>" name="link"><br/>
				<a onclick="return confirm('Do you want to delete <?=$partner->name?>?')"  class="btn btn-danger" href="<?=base_url()?>admin/delete_partner/<?=$partner->id?>">Delete</a>
				<input type="hidden" name="partner_id" value="<?=$partner->id?>">
				<input type="submit" class="btn btn-primary"  value="Save partner" name="save_partner">
			</form>
		</div>
		<?php endforeach;?>
	</div><!-- /#page-wrapper -->
<?=$footer?>