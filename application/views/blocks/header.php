<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title><?=($title)?$title:'Wepromis.eu - vote for your digital rights'?></title>
		<meta name="description" content="<?=$description?>" >
		<meta name="keywords" content="<?=$keywords?>" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="<?=base_url()?>favicon.ico" />

		<!-- Bootstrap -->
		<link href="<?=base_url();?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url();?>assets/frontend/css/main.css" rel="stylesheet">


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		  	<link href="<?=base_url();?>assets/frontend/css/ie8.css" rel="stylesheet">
		<![endif]-->
		
		<!--[if gte IE 9]>
				<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/frontend/css/ie9.css" />
		<![endif]-->
	</head> 
	<body>

		<div class="container" id="header">
			<div class="row">
				<div class="col-sm-3">
					
					<a id="mobile_menu" >
						<img src="<?=base_url()?>assets/frontend/img/mobile_menu.png" />
					</a>
					
					
					<div id="mobile_menu_box" >
						<div class="mm_wrapper">
							<?php foreach($pages as $page):?><a href="<?=base_url()?><?=$this->uri->segment(1)?>/page/<?=$page->slug?>"><?=$page->anchor?></a><?php endforeach;?>
						</div><!--
					 --><div id="close_menu_btn"></div>
					
					</div>
				 
					<a href="<?=base_url()?>"><img id="logo" alt="logo" src="<?=base_url()?>assets/frontend/img/logo.png" /></a>


					<div class="pull-right   visible-xs " >
						<div class="lang_switch clickable">
							<?=strtoupper($this->uri->segment(1));?>

							<div class="options">
								<?php $i=0; foreach($languages as $lang): ?>
									<?php 
									
										$link =""; 
										if($this->uri->segment(3)!="") $link = base_url().$lang->language_code."/".$this->uri->segment(2)."/".$this->uri->segment(3);
										else $link = base_url().$lang->language_code;
									
									?>
									<a href="<?=$link?>"><?=$lang->name?></a>
									<?php if($i<count($languages)) echo '<hr/>';?>
								<?php endforeach;?>
							</div>
 

						</div>
					</div>

				</div>
				
				<div class="pull-right col-sm-1 hidden-xs" >
					<div class="lang_switch clickable">
						<?=strtoupper($this->uri->segment(1));?>

						<div class="options">
							<?php $i=0; foreach($languages as $lang): $i++; ?>
							
								<?php 
									
									$link ="";  
									if($this->uri->segment(3)!="") {
											if($this->uri->segment(2)=='page') $link = base_url().$lang->language_code."/".$this->uri->segment(2)."/".$other_slugs[$lang->language_code];
											else  $link = base_url().$lang->language_code."/".$this->uri->segment(2)."/".$this->uri->segment(3);
									}
									else $link = base_url().$lang->language_code;
								
								?>
								<a href="<?=$link?>"><?=$lang->name?></a>
								<?php if($i<count($languages)) echo '<hr/>';?>
							<?php  endforeach;?>
						</div>
 

					</div>
				</div> 


				<div class="web_menu col-sm-8  hidden-xs ">

				<?php foreach($pages as $page):?><a class="item" href="<?=base_url()?><?=$this->uri->segment(1)?>/page/<?=$page->slug?>"><?=$page->anchor?></a><?php endforeach;?>
				</div>




			</div>
		</div>