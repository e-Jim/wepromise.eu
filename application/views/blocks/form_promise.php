<div   class="promise_form_container ">
			<div class="container">
			<?php if(!$this->session->flashdata('vp_confirmation_response') == "success"): ?>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-text">
						
 
						
						
							<?=$content->promise_to_vote_intro?> <span class="em1"><?=$content->point_charter?></span>  
							<span class="em2">
								<?=$content->digital_rights?>
								<div class="c_tooltip <?=($reversed?"reversed":"")?>">
									<span class="c_close">&times;</span>
								
									<span class="title"><?=$promises?></span>
									<span class="label"><?=$content->promised_to_vote?></span>
									<span class="nub">
										<span class="nub_shadow"></span>
									</span>
								</div>
							</span>
						</div>
					</div>

				</div>
 
			
				<div class="row promisse_form">
					<form  id="promise_to_vote_form" method="post" action="<?=base_url()?>auxiliar/handle_vote_promise">
						<div class="col-sm-3">
							<input name="name" type="text" placeholder="<?=$content->name_placeholder?>" />
						</div>
						<div class="col-sm-3">
							<input type="text" name="email" placeholder="E-mail" />
						</div>
						<div class="col-sm-3">
								<div class="pseudo_select">
									<input type="hidden" name="country" value="" />
									<span class="screen"><?=$content->country_placeholder?></span>
									
									<div class="options">
										<div data-value=""><?=$content->country_placeholder?></div>
										<div data-value="at">Österreich</div>
										<div data-value="be">Belgique/België</div>
										<div data-value="bg">Bulgaria</div>
										<div data-value="hr">Hrvatska</div>
										<div data-value="cy">Cyprus</div>
										<div data-value="cz">Česká republika</div>
										<div data-value="dk">Denmark</div>
										<div data-value="ee">Eesti</div>
										<div data-value="fi">Suomi</div>
										<div data-value="fr">France</div>
										<div data-value="de">Deutschland</div>
										<div data-value="gr">Greece</div>
										<div data-value="hu">Magyarország</div>
										<div data-value="ie">Republic of Ireland</div>
										<div data-value="it">Italia</div>
										<div data-value="lv">Latvija</div>
										<div data-value="lt">Lietuva</div>
										<div data-value="lu">Luxembourg</div>
										<div data-value="mt">Malta</div>
										<div data-value="nl">Nederland</div>
										<div data-value="pl">Polska</div>
										<div data-value="pt">Portugal</div>
										<div data-value="ro">România</div>
										<div data-value="sk">Slovensko</div>
										<div data-value="si">Slovenija</div>
										<div data-value="es">Espana</div>
										<div data-value="se">Sverige</div>
										<div data-value="gb">United Kingdom</div>
									</div>
								</div>
						</div>
					
						<div class="col-sm-3">
							<input class="clickable" type="submit" value="<?=$content->promise_to_vote?>" /> 
							
							<input id="also_subscribe_newsletter" name="subscribe" value="subscribe" type="checkbox" > 
							
							<label for="also_subscribe_newsletter">	
								<div class="table_cell">
									<div id="the_checkbox" class="">
										
									</div>
								</div>
								<div class="table_cell">
									<div class="receive_newsletter">
										<?=$content->receive_newsletter?>
									</div>
								</div>
							</label>
							
						</div>
					
					</form>

				</div> 
				<?php endif;?>
				<div class="row message_succes">
					<div class="col-sm-12">
						<?=$content->success_message?>
					</div>
				</div>
				<div class="row message_error">
					<div class="col-sm-12">	
				
						<div class="name_required">
							<?=$content->name_required?>
						</div>	
						<div class="email_required" style="display:none">
							<?=$content->email_required?>
						</div> 
						<div class="country_required" style="display:none">
							<?=$content->country_required?>
						</div>
					

					</div>
				</div>				
				<div class="row message_error_email">
					<div class="col-sm-12"> 
						<?=$content->duplicate_email?>
					</div>
				</div>
				<div class="row please_validate_email">
					<div class="col-sm-12">
						<?=$content->please_validate_email?>
					</div>
				</div>	
		
				<?php if($this->session->flashdata('vp_confirmation_response') == "success"): ?>
					<div class="row message_succes force_display">
						<div class="col-sm-12">
							<?=$content->success_message?>
						</div>
					</div>
				<?php endif; ?>
				
				<?php if($this->session->flashdata('vp_confirmation_response') == "fail"): ?>
					<div class="row message_error force_display">
						<div class="col-sm-12">
							<?=$content->validation_error?>
						</div> 
					</div>
				<?php endif; ?>
				<?php //$this->session->sess_destroy();?>
			</div>  
		</div>
