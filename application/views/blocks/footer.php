		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="<?=base_url();?>assets/frontend/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?=base_url();?>assets/frontend/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/jquery.placeholder.js"></script>
		
		<script type="text/javascript">
		$( document ).ready(function() {
			$('input, textarea').placeholder();
		});
				
		
		</script>
		<?=$footer_scripts?>


		

	</body>
</html>