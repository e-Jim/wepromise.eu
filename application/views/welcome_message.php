<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

/* custom font! */
/* ------------ */


/* scaffolding */
/* ----------- */

html {
  overflow-y: scroll;
  *overflow-x: hidden;
}

.container {
  max-width: 700px;
  margin: 0 auto;
  text-align: center;
}

.tt-dropdown-menu,
.gist {
  text-align: left;
}

/* base styles */
/* ----------- */

html {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
  line-height: 1.2;
  color: #333;
}

.title,
.example-name {

}

p {
  margin: 0 0 10px 0;
}

/* site theme */
/* ---------- */

.title {
  margin: 20px 0 0 0;
  font-size: 64px;
}

.example {
  padding: 30px 0;
}

.example-name {
  margin: 20px 0;
  font-size: 32px;
}

.demo {
  position: relative;
  *z-index: 1;
  margin: 50px 0;
}

.typeahead,
.tt-query,
.tt-hint {
  width: 396px;
  height: 30px;
  padding: 8px 12px;
  font-size: 24px;
  line-height: 30px;
  border: 2px solid #ccc;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  outline: none;
}

.typeahead {
  background-color: #fff;
}

.typeahead:focus {
  border: 2px solid #0097cf;
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-dropdown-menu {
  width: 422px;
  margin-top: 12px;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion.tt-is-under-cursor {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}

.gist {
  font-size: 14px;
}

/* example specific styles */
/* ----------------------- */

.example-twitter-oss .tt-suggestion {
  padding: 8px 20px;
}

.example-twitter-oss .tt-suggestion + .tt-suggestion {
  border-top: 1px solid #ccc;
}

.example-twitter-oss .repo-language {
  float: right;
  font-style: italic;
}

.example-twitter-oss .repo-name {
  font-weight: bold;
}

.example-twitter-oss .repo-description {
  font-size: 14px;
}

.example-sports .league-name {
  margin: 0 20px 5px 20px;
  padding: 3px 0;
  border-bottom: 1px solid #ccc;
}

.example-arabic .tt-dropdown-menu {
  text-align: right;
}

	</style>

</head>
<body>

<div id="container">
<div class="example example-sports">
      
        <div class="demo">
          <input class="typeahead" type="text" name="typeahead" placeholder="City, zipcode or district">
        </div>


      </div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

	<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/typeahead/dist/typeahead.min.js"></script>
	<script type="text/javascript">
	//var cities,districts="";
	//$.get('http://webfoo.ro/projects/parlamentari/site/welcome/results', function(data) {
	//	var cities = data;
	//});	
	//$.get('http://webfoo.ro/projects/parlamentari/site/welcome/districts', function(data) {
	//	var districts = JSON.parse(''+data+'');
	//});
	$( document ).ready(function() {
/*	
	var myTypeahead = $('.example-sports .typeahead').typeahead(
	 [
		{
			remote: {
				url: 'http://webfoo.ro/projects/parlamentari/site/welcome/cities/',
				replace: function () {
					var q = 'http://webfoo.ro/projects/parlamentari/site/welcome/cities/';
					if ($('.typeahead').val()) {
						q += encodeURIComponent($('.typeahead').val());
					}
					return q;
				},
				filter: function(parsedResponse) {
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens
					  });
					}
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No-results" 
					  });
					}
					return dataset;
				},
			},

			cache: false,
			limit: 10
        },
		{
			remote: {
				url: 'http://webfoo.ro/projects/parlamentari/site/welcome/zipcodes/',
				replace: function () {
					var q = 'http://webfoo.ro/projects/parlamentari/site/welcome/zipcodes/';
					if ($('.typeahead').val()) {
						q += encodeURIComponent($('.typeahead').val());
					}
					return q;
				},
				filter: function(parsedResponse) {
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens
					  });
					}
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No-results" 
					  });
					}
					return dataset;
				},
			},
			cache: false,
			limit: 10
        }		
	]
	);	
*/
	var myTypeahead = $('.example-sports .typeahead').typeahead(

		{
			remote: {
				url: 'http://webfoo.ro/projects/parlamentari/site/welcome/search_results/',
				replace: function () {
				
					var q = 'http://webfoo.ro/projects/parlamentari/site/welcome/search_results/';
					if ($('.typeahead').val()) {
						q += encodeURIComponent($('.typeahead').val());
					}
					return q;
				
				}
				/*
				,
				
				filter: function(parsedResponse) { 
					var dataset = [];
					for (i = 0; i < parsedResponse.length; i++) {
					  dataset.push({
						value: parsedResponse[i].value,
						tokens: parsedResponse[i].tokens
					  });
					}
					if (parsedResponse.length == 0) {
					  dataset.push({
						value: "No-results" 
					  });
					}
					return dataset;
				},
				
				*/
			}, 

			cache: false,
			limit: 10
        }
	);
	myTypeahead.on('typeahead:selected',function(evt,data){
		console.log('data==>' + data.zip_id); //selected datum object
		console.log('data==>' + data.city_id); //selected datum object
		console.log('value==>' +data.tokens);
	});
	}); 
		
		</script>
</body>
</html>