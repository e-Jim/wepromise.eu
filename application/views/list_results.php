<?=$header?>
<div class="results_page">

			<?=$form_promise?>
			
			
			
			
			
			<div class="search_strip">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="search_quote">
								<strong><?=count($candidates)?> <?=$content->results_for?></strong>&nbsp; &ldquo;<span class="quote"><?=$search_term?></span>&rdquo;  
								
							</div>
						</div>
						
						<!--div class="col-md-1 visible-md visible-lg">  </div--> <!-- offset hack -->
						 
						<div class="col-md-4 relative_positioned">
							<div>
								<div class="focusable_child" id="input_wrapper">
									<input type="text" class="typeahead" placeholder="<?=$content->search_placeholder?>" />
									<input type="submit" value="" />

								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			
			
			<div id="results_container">
			<div class="container">
				<!-- ########################################################################################  set of 4 results -->			
				<?php $i=0; $j=0; foreach($candidates as $candidate): $i++; ?>
					<?php if($i==1):?>
						<div class="row">
					<?php endif;?>
				
							<div class="col-sm-6 col-md-3">
								<div class="result">
								<?php if($candidate->winner=="activ"):?>
									<div class="abso">
										Elected digital rights superhero
									</div>
								<?php endif;?>
									<div  class="title flag-<?=$candidate->country_code?>">
										<?php if($candidate->city!="") echo $candidate->city.'<br />' ;?>
										<?php if($candidate->district!=""&&$candidate->district!="Empty") echo $candidate->district.'<br />' ;?>
										<?=$candidate->country?>
									</div>
									
									<div class="name"><?=$candidate->name?> </div>
									<?php //if($candidate->party_name||$candidate->website):?>
										<div class="party">
											<?php if($candidate->party_name!="") echo $candidate->party_name.'<br />' ;?>
											<?php if($candidate->website!=""):?>
											<a href="<?=$candidate->website?>" target="_blank">Website</a><br/>
											<?php endif;?>
											<?php if($candidate->youtube!=""):?>
											<a href="<?=$candidate->youtube?>" target="_blank">Statement</a>
											<?php endif;?>
										</div> 
									<?php // endif;?>
									<?php 
										$desc_value = "desc_".$language_id; 
										if($candidate->$desc_value !="") $description = $candidate->$desc_value; else $description = $candidate->desc_6;
										echo "<p>".$description."</p>";
									?>
									 
									<div class="social">
										<div class="row">
										<?php if($candidate->twitter!=""):?>
											<div class="col-xs-4 " >
												<a class="tw" target="_blank" href="<?=$candidate->twitter?>"></a>
											</div>
										<?php endif;?>
										<?php if($candidate->facebook!=""):?>
											<div class="col-xs-4 text_align_center" >
												<a class="fb" target="_blank" href="<?=$candidate->facebook?>"></a>
											</div>
										<?php endif;?>
										<?php if($candidate->gplus!=""):?>
											<div class="col-xs-4 text_align_center" >
												<a class="gplus" target="_blank"  href="<?=$candidate->gplus?>"></a>
											</div>
										<?php endif;?>
											<!--
											<div class="col-xs-3 text_align_right" >
												<a class="mail" href="#"></a>
											</div>
											--> 
										</div>
									</div>
								</div>
							
							
							</div>
					<?php if($i==count($candidates)): $j++;?>	
						</div>
						<div class="result_section_delimiter row">
							<?=$content->results_section?> <?=$j?>
						</div>
						
					<?php endif;?>
						

					<?php  if($i%4==0 && $i<count($candidates)): $j++?>	
						</div>
						<div class="result_section_delimiter row">
							<?=$content->results_section?> <?=$j;?>
						</div>
						<div class="row">
					<?php endif;?>				
					
					<?php endforeach;?>		
				</div>	
			</div>
			
		</div>
		
		<div id="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="title"><?=$content->spread_news?></div>
					</div>


						<?=$sociale_footer?>

					<div class="col-md-3">
						
						<a target="_blank" href="<?=base_url()?>uploads/support_kits/<?=$support_kit?>" class="clickable green_button"><?=$content->download_kit?></a>
					</div>
				</div>
			</div>

		</div>

		<div id="footer_bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12"><?=$content->footer_copyright?></div>
				</div>
			</div>


		</div>
		

<?=$footer?>