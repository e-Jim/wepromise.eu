<?=$header?>

		<div class="results_page">

			<?=$form_promise?>
			
			
			
			
			
			<div class="search_strip">
				<div class="container">
					<div class="row"> 
						
						 
						<div class="col-md-offset-3 col-md-6 relative_positioned">
							<div class="text-center"><?=$content->text_below_search_cms?>

							</div>
							
						</div>
					</div>				
					<div class="row">
						
						 
						<div class="col-md-offset-3 col-md-6 relative_positioned">
						
								<div class="focusable_child" id="input_wrapper">
									<input type="text" class="typeahead" placeholder="<?=$content->search_placeholder?>" />
									<input type="submit" value="" />

								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		 
		<div class="text_page">
				
				
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="text_page_block">
						
							<h1 class="title"><?=$content_page->anchor?></h1>
							<?php if($tabs==true):?>
								<?php 				


//$text = iconv("UTF-8", "UTF-8", $content_page->content);
//$dom = new SmartDOMDocument();
//$dom->loadHTML($text, 'UTF-8');		
$html = mb_convert_encoding($content_page->content, 'HTML-ENTITIES', "UTF-8");						
									$dom = new domDocument('1.0', 'UTF-8'); 
									 // load the html into the object ***/ 
									// $dom->loadHTML(utf8_decode( $content_page->content)); 
									 $dom->loadHTML($html); 
									 //discard white space 
									 $dom->preserveWhiteSpace = false; 
									 $nodes_h4= $dom->getElementsByTagName('h4');
									 $nodes_h5= $dom->getElementsByTagName('h5');
									 $nodes_h6= $dom->getElementsByTagName('h6');
									 
									 $i=0;
								?>		

								<p><?=$nodes_h6->item(0)->nodeValue?></p>
								<div class="panel-group" id="accordion">
									<?php for($i==0;$i<$nodes_h4->length;$i++):?>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>">
														<?=$nodes_h4->item($i)->nodeValue?>
													</a>
												</h4> 
											</div>

											
													
											<div id="collapse<?=$i?>" class="panel-collapse collapse <?=($i==0)?'in':'';?>">
												<div class="panel-body">
													<?=$nodes_h5->item($i)->nodeValue?>
												</div>
											</div>
										

										</div>
									
									<?php endfor; ?>
								</div>	
								
							<?php else: ?>
								<div class="row">
								<?php if($content_page->content_2==""&&$content_page->content_3==""&&$content_page->content!=""):?>
									<div class="col-sm-12">
										<?=$content_page->content?>
									</div>
								<?php endif;?>
								<?php if($content_page->content_2!=""&&$content_page->content!=""):?>		
									<div class="col-sm-4">
										<?=$content_page->content?>
									</div>
									<div class="col-sm-4">
										<?=$content_page->content_2?>
									</div>
									<div class="col-sm-4">
										<?=$content_page->content_3?>
									</div>
								<?php endif;?>
								</div>
							<?php endif;?>
							
							
							<?php if($partners):?>
								<div class="row">
								
							<?php						
								foreach($partners as $partner):		
							?>
							<div class="col-sm-2"> 
								<div class="partner_container">
									<div class="partner_logo">
										<a target="_blank" href="<?=$partner->link?>"><img class="img-responsive" src="<?=base_url()?>/uploads/partners/<?=$partner->image?>"></a>
									</div>
									<div class="partner_link">
										<a target="_blank" href="<?=$partner->link?>"><?=$partner->name?></a>
									</div>
								</div>
							</div>
							<?php endforeach;?>
								
								
								
								</div>
							
							
							<?php endif;?>
						
						
						
						
						</div>
					
					
					</div>
				</div>
			</div>
		</div>
		
		<div id="footer_top">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="title"><?=$content->spread_news?></div>
					</div>


						<?=$sociale_footer?>

					<div class="col-md-3">
						
						<a target="_blank" href="<?=base_url()?>uploads/support_kits/<?=$support_kit?>" class="clickable green_button"><?=$content->download_kit?></a>
					</div>
				</div>
			</div>

		</div>

		<div id="footer_bottom">
			<div class="container">
				<div class="row">
					<div class="col-xs-12"><?=$content->footer_copyright?></div>
				</div>
			</div>


		</div>
		

<?=$footer?>