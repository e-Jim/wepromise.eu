<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Edri_front extends CI_Model
{
	function __construct()
	{
		parent::__construct();
 
	}
	function get_countries(){
	
		return  $this->db->get('countries')->result();
	}
	
	function country_codes($countries){
		$country_codes = array();
		foreach($countries as $country) {
			$country_codes[] = strtoupper($country->code);
		}
		return $country_codes;
	}
	
	function get_country_candidates($countries){
		
		foreach($countries as $country){
			$country->nr_candidates = count($this->db->get_where('candidates',array('country_id'=>$country->id))->result());
			 
		}
		return $countries;
	}
	function check_country_active_language($country_code){
		$country_code = strtolower($country_code);
		$country = $this->db->get_where('countries',array('code'=>$country_code))->row();
		
		$this->db->where('status','true');
		$this->db->where('id',$country->language_id);
		$query = $this->db->get('languages');
		if($query->num_rows()==1) 
		return true;
		else return false;
	}
	
	function get_language_code_by_country_code($country_code){
		
		$language_id = $this->db->get_where('countries',array('code'=>$country_code))->row()->language_id;
		$language_code  = $this->db->get_where('languages',array('id'=>$language_id))->row()->language_code;
		return $language_code;
	}
	function language_id_from_code($language){
		return $this->db->get_where('languages',array('language_code'=>$language))->row();
	}
	function get_translations($language_id){
		
		return $this->db->get_where('translations',array('language_id'=>$language_id))->row();
	}
	
	function get_active_languages(){
		$this->db->where('status','true');
		return $this->db->get('languages')->result();
	}	
	function get_pages($language_id){
		$pages = $this->db->get('pages')->result();
		$rez = array();
		foreach($pages as $page){
			$this->db->where('page_id',$page->id);
			$this->db->where('language_id',$language_id);
			$rez[] = $this->db->get('page_translations')->row();
		}
		return $rez;
	}
	
	function register_vote_promise($data){
	
		if($this->db->get_where('vote_promises',array('email' => $data->email))->num_rows() > 0) {
			return false;
		}
		return $this->db->insert('vote_promises',$data);
	}
	function get_nr_promises(){
		$this->db->where('validated','yes');
		return 	$this->db->get('vote_promises')->num_rows();
	}
		
	function get_candidates_by_zipcode($zip_id){
		$district_id = $this->db->get_where('district_zipcode',array('zipcode_id'=>$zip_id))->row()->district_id;
		$city_id 	 = 	$this->db->get_where('districts',array('id'=>$district_id))->row()->city_id;
		$country_id  = 	$this->db->get_where('cities',array('id'=>$city_id))->row()->country_id;
		
		//$cities = $this->db->get('cities')->result();
		$countries = $this->db->get('countries')->result();
		//$districts = $this->db->get('districts')->result();
		//var_dump($country_id);
	//	$this->db->where('country_id',$country_id);
	//	$this->db->where('district_id',$district_id);
	//	$this->db->or_where('district_id','0');
		
		 
		$this->db->where("(district_id = '$district_id' OR district_id = '0') AND country_id = '$country_id' AND status = 'activ'");
		
		$candidates = $this->db->get('candidates')->result();
		foreach ( $candidates as $candidate ){
			//$candidate->city 			= (array_key_exists($candidate->city_id-1,$cities))?$cities[$candidate->city_id-1]->name:'';
			//$candidate->district 		= (array_key_exists($candidate->district_id-1,$districts))?$districts[$candidate->district_id-1]->name:'';
			$candidate->city 		 = ($this->get_cityby_id($candidate->city_id))?$this->get_cityby_id($candidate->city_id):'';
			$candidate->district 	 = ($this->get_districtby_id($candidate->district_id))?$this->get_districtby_id($candidate->district_id):'';			
			$candidate->country 		= $countries[$candidate->country_id-1]->name;
			$candidate->country_code    = $countries[$candidate->country_id-1]->code;
		}
		return $candidates;	 
	}
	
	function get_zip_search_term($zip_id){
	
		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,zipcodes.zip,zipcodes.id as zip_id');
		$this->db->join('cities','cities.id=zipcodes.city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->where('zipcodes.id',$zip_id);
		$row = $this->db->get('zipcodes')->row();
		
		return $row->zip.', '.$row->city_name.', '.$row->country_name;
	}
	function get_candidates_by_city($city_id){
		$country_id  = 	$this->db->get_where('cities',array('id'=>$city_id))->row()->country_id;
		//$cities = $this->db->get('cities')->result();
		$countries = $this->db->get('countries')->result();
		//$districts = $this->db->get('districts')->result();
		
		//$this->db->where('country_id',$country_id);
		//$this->db->where('city_id',$city_id);
		//$this->db->or_where('city_id','0');
		$this->db->where("(city_id = '$city_id' OR city_id = '0') AND country_id = '$country_id' AND status = 'activ'");
		$candidates = $this->db->get_where('candidates')->result();
		

		foreach ( $candidates as $candidate ){
			
			//$candidate->city 		 = (array_key_exists($candidate->city_id-1,$cities))?$cities[$candidate->city_id-1]->name:'';
			//$candidate->district 	 = (array_key_exists($candidate->district_id-1,$districts))?$districts[$candidate->district_id-1]->name:'';
			$candidate->city 		 = ($this->get_cityby_id($candidate->city_id))?$this->get_cityby_id($candidate->city_id):'';
			$candidate->district 	 = ($this->get_districtby_id($candidate->district_id))?$this->get_districtby_id($candidate->district_id):'';
			
			$candidate->country 	 = $countries[$candidate->country_id-1]->name;
			$candidate->country_code = $countries[$candidate->country_id-1]->code;
		}
		return $candidates;
	}	

	function get_city_search_term($city_id){
		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->where('cities.id',$city_id);
		$row = $this->db->get('cities')->row();
		
		return  $row->city_name.', '.$row->country_name;
	}
	
	 
	function get_candidates_by_district($district_id){
	
		$city_id 	 = 	$this->db->get_where('districts',array('id'=>$district_id))->row()->city_id;
		$country_id  = 	$this->db->get_where('cities',array('id'=>$city_id))->row()->country_id;
	
		//$cities = $this->db->get('cities')->result();
		$countries = $this->db->get('countries')->result();
		//$districts = $this->db->get('districts')->result();
	
		//$this->db->where('country_id',$country_id);
		//$this->db->where('district_id',$district_id);
		//$this->db->or_where('district_id','0');
		$this->db->where("(district_id = '$district_id' OR district_id = '0') AND country_id = '$country_id' AND status = 'activ'");
		$candidates = $this->db->get('candidates')->result();
		foreach ( $candidates as $candidate ){
			//$candidate->city 			= (array_key_exists($candidate->city_id-1,$cities))?$cities[$candidate->city_id-1]->name:'';
			//$candidate->district 		= (array_key_exists($candidate->district_id-1,$districts))?$districts[$candidate->district_id-1]->name:'';
			$candidate->city 		 = ($this->get_cityby_id($candidate->city_id))?$this->get_cityby_id($candidate->city_id):'';
			$candidate->district 	 = ($this->get_districtby_id($candidate->district_id))?$this->get_districtby_id($candidate->district_id):'';			
			$candidate->country		    = $countries[$candidate->country_id-1]->name;
			$candidate->country_code    = $countries[$candidate->country_id-1]->code;
		}
		return $candidates;		
		
		
	}
	function get_district_search_term($district_id){
		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,districts.name as district_name ,districts.id as district_id');
		$this->db->join('cities','cities.id=districts.city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->where('districts.id',$district_id);
		$row = $this->db->get('districts')->row();		
		
		return $row->district_name.', '.$row->city_name.', '.$row->country_name;
	
	}
	
	
	function get_candidates_by_country($country_id){
		$countries = $this->db->get('countries')->result();
		$this->db->where('country_id',$country_id);
		$candidates = $this->db->get('candidates')->result();
		foreach ( $candidates as $candidate ){
			$candidate->city 		 = ($this->get_cityby_id($candidate->city_id))?$this->get_cityby_id($candidate->city_id):'';
			$candidate->district 	 = ($this->get_districtby_id($candidate->district_id))?$this->get_districtby_id($candidate->district_id):'';			
			$candidate->country		    = $countries[$candidate->country_id-1]->name;
			$candidate->country_code    = $countries[$candidate->country_id-1]->code;
		}
		return $candidates;		
		
	}
	function get_country_search_term($country_id){
		$this->db->where('id',$country_id);
		$row = $this->db->get('countries')->row();	
		return $row->name;
	}
	function get_cityby_id($city_id){
		$this->db->where('id',$city_id);
		$query = $this->db->get_where('cities');
		if($query->num_rows()==1) return $query->row()->name;
		else return false;
	}
	function get_districtby_id($city_id){
		$this->db->where('id',$city_id);
		$query = $this->db->get_where('districts');
		if($query->num_rows()==1) return $query->row()->name;
		else return false;
	}		
	function get_content_page($language_id,$slug){
	
		return $this->db->get_where('page_translations',array('language_id'=>$language_id,'slug'=>$slug))->row();
	
	}
	 
	function get_recent_candidates(){
	
		//$cities = $this->db->get('cities')->result();
		$countries = $this->db->get('countries')->result();
		//$districts = $this->db->get('districts')->result();
		$this->db->order_by('id','desc');
		$this->db->limit(15);
		$candidates = $this->db->get('candidates')->result();
		foreach ( $candidates as $candidate ){
			$candidate->city 		 = ($this->get_cityby_id($candidate->city_id))?$this->get_cityby_id($candidate->city_id):'';
			$candidate->district 	 = ($this->get_districtby_id($candidate->district_id))?$this->get_districtby_id($candidate->district_id):'';	
			$candidate->country		    = $countries[$candidate->country_id-1]->name;
			$candidate->country_code    = $countries[$candidate->country_id-1]->code;
		}
		return $candidates;		
	
	
	}

	
	function get_other_slugs($slug,$languages){
		$other_slugs = array();
		$page_id 			= $this->db->get_where('page_translations',array('slug'=>$slug))->row()->page_id;
		$all_translations   = 	$this->db->get_where('page_translations',array('page_id'=>$page_id))->result();
		
		foreach($all_translations as $translation){
			  
			foreach($languages as $lang){
			
				if($translation->language_id==$lang->id) 
					$other_slugs[$lang->language_code] = $translation->slug;
			
			}
		}
		return $other_slugs;
	}
	
	function send_confirm_promise_email($email,$name){
		
		$promise_id = $this->db->insert_id();
		$unique_id =  uniqid();
		
		$this->db->insert('flash_codes', array("code"=>$unique_id, "data"=>$promise_id, "expired"=>"0"));		

		
	//	$this->load->library('email');
	//	$config['mailtype'] = 'html';
	//	$config['charset'] = 'utf-8';
	//	$config['wordwrap'] = TRUE;
	//	$this->email->initialize($config);

		$config=array( 'mailtype' => 'html','charset'=>'UTF-8', 'wordwrap'=>TRUE );
		$this->load->library('email',$config);
		
		$data['email'] = $email;
		$data['name'] = $name;
		$data['link'] = base_url() . 'confirm_vote_promise/'.$unique_id;
		
		
		$message = $this->load->view('confirm_email_ptv',$data, true);
		

		$this->email->from("info@wepromise.eu", "We Promise");
		
		$this->email->to($email); 

		  
		$this->email->subject("Please Confirm Your Promise");
		$this->email->message($message);
		$result = $this->email->send();
	//	echo $this->email->print_debugger();
	}
	function send_confirm_promise_email_manual(){
		$email 			= 'nesnera@email.cz';
		$email_admin 	= 'leonte.marian@gmail.com';
		$name 			= 'Ladislav Nesnera';
		$promise_id 	= '3597';
		$unique_id 		= '53721e63809e5';
		
		
		$config=array( 'mailtype' => 'html','charset'=>'UTF-8', 'wordwrap'=>TRUE );
		$this->load->library('email',$config);
		
		$data['email'] = $email;
		$data['name'] = $name;
		$data['link'] = base_url() . 'confirm_vote_promise/'.$unique_id;
		
		
		$message = $this->load->view('confirm_email_ptv',$data, true);
		

		$this->email->from("info@wepromise.eu", "We Promise");
		
		$this->email->to($email); 
		$this->email->bcc($email_admin); 

		  
		$this->email->subject("Please Confirm Your Promise");
		$this->email->message($message);
	//	$result = $this->email->send();
	//	echo $this->email->print_debugger();
	}
}