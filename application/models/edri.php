<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Edri extends CI_Model
{
	function __construct()
	{
		parent::__construct();
 
	}
	
 	function get_languages(){
		return $this->db->get('languages')->result();
	}	
	
	function get_active_languages(){
	//	$this->db->where('status','true');
		$this->db->order_by('order_id','asc');
		return $this->db->get('languages')->result();
	}	
	
	function get_countries(){
		return $this->db->get('countries')->result();
	}
	

	
	function get_countries_and_cities(){
		$countries = $this->db->get('countries')->result();
		foreach($countries as $country){
			$this->db->where('country_id',$country->id);
			$this->db->where('type','added');
			$country->cities = $this->db->get('cities')->result();	
		}
		
		return $countries;
	}
	
	function get_cities(){
		return $this->db->get('cities')->result(); 
	}	
	
	function get_city($city_id){
		return $this->db->get_where('cities',array('id'=>$city_id))->row();
	}
	
	function get_city_districts($city_id){
		//$this->db->join('citi_district','citi_district.district_id=districts.id');
		//$this->db->where('citi_district.city_id',$city_id);
		$this->db->where('city_id',$city_id);
		$districts = $this->db->get('districts')->result();
		
		foreach($districts as $district){
			$this->db->join('district_zipcode','district_zipcode.zipcode_id=zipcodes.id');
			$this->db->where('district_zipcode.district_id',$district->id);
			$this->db->where('zipcodes.city_id',$city_id);
			$district->zipcodes = $this->db->get('zipcodes')->result(); 	
		}
		return $districts; 
	}
	
	function get_districts(){ 
		return $this->db->get('districts')->result();
	}	
	 
	function get_candidates(){
		$countries  = $this->fetch_array($this->get_countries(),'name'); 
		//$cities 	= $this->fetch_array($this->get_cities(),'name');
		//$districts  = $this->fetch_array($this->get_districts(),'name');
	
	
		$candidates = $this->db->get('candidates')->result();
		foreach($candidates as $candidate){
			$candidate->country		=	$countries[$candidate->country_id];
			
			
			//$candidate->city		=	(array_key_exists($candidate->city_id,$cities))?$cities[$candidate->city_id]:'All cities';
			
			$candidate->city		=	$this->get_city_by_cityID($candidate->city_id);
			$candidate->district	=	$this->get_district_by_districtID($candidate->district_id);
			//$candidate->district	=	(array_key_exists($candidate->district_id,$districts))?$districts[$candidate->district_id]:'All districts';
			//$candidate->district	=	(array_key_exists($candidate->district_id,$districts))?$districts[$candidate->district_id]:'All districts';
		}
		return $candidates;
	}
	function get_city_by_cityID($city_id){
		$query = $this->db->get_where('cities',array('id'=>$city_id));
		if($query->num_rows()==1) return $query->row()->name;
		else return 'All cities';
	
	}
	function get_district_by_districtID($district_id){
		$query = $this->db->get_where('districts',array('id'=>$district_id));
		if($query->num_rows()==1) return $query->row()->name;
		else return 'All districts';
	
	}
	function get_zip($zip_id){
		return $this->db->get_where('zipcodes',array('id'=>$zip_id))->row();
	}
 	
	function delete_zipcode($zip_id){
		$this->db->where('id',$zip_id);
		$this->db->delete('zipcodes');
		
	}
	
	function insert_city($country_id,$data){
		$this->db->insert('cities',array('country_id'=>$country_id,'name'=>$data['city_name'],'type'=>'added'));
		$city_id = $this->db->insert_id();
		for($i=1;$i<=$data['count'];$i++){

		
			$this->db->insert('districts',array('name'=>$data['district'.$i],'city_id'=>$city_id));
			$district_id = $this->db->insert_id();

			
			$zips_for_district = explode(',',$data['zipcodes'.$i]);
			foreach($zips_for_district as $the_zip){
				//check for ranges
				$ranges = explode('range',$the_zip);
				if(count($ranges)>1){
					$from = $ranges[0];
					$to	  = $ranges[1];
					for($from;$from<=$to;$from++){
						$this->db->insert('zipcodes',array('zip'=>$from,'city_id'=>$city_id));
						$zipcode_id = $this->db->insert_id();
						$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$district_id));
					} 
				
				}
				else {
			 
					$this->db->insert('zipcodes',array('zip'=>$the_zip,'city_id'=>$city_id));
					$zipcode_id = $this->db->insert_id();
					$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$district_id));
				}
			}
		}
	}

	function insert_new_districts($city_id,$data){
			
		for($i=1;$i<=$data['count'];$i++){ 
			$this->db->where('city_id',$city_id);
			$this->db->where('name',$data['district'.$i]);
			$query = $this->db->get('districts');
			 
			if($query->num_rows()==0) {
				$this->db->insert('districts',array('name'=>$data['district'.$i],'city_id'=>$city_id));
				$district_id = $this->db->insert_id();
				//$this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));			
			}
			else{   
				$district_id = $query->row()->id;
			//	$this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));
			}
			
			$zips_for_district = explode(',',$data['zipcodes'.$i]);
			foreach($zips_for_district as $the_zip){
				//check for ranges
				$ranges = explode('range',$the_zip);
				if(count($ranges)>1){
					$from = $ranges[0];
					$to	  = $ranges[1];
					for($from;$from<=$to;$from++){
						$this->db->insert('zipcodes',array('zip'=>$from,'city_id'=>$city_id));
						$zipcode_id = $this->db->insert_id();
						$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$district_id));
					} 
				
				}
				else {
					if($the_zip!=""){
						$this->db->insert('zipcodes',array('zip'=>$the_zip,'city_id'=>$city_id));
						$zipcode_id = $this->db->insert_id();
						$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$district_id));
					}
				}
			}
		}
	}
	
	function update_districts_name($city_id,$data){
	
		unset($data['send']);
		foreach($data as $row=>$value){
		
			$this->db->where('id',str_replace('district','',$row));
			$this->db->update('districts',array('name'=>$value));
		
		}
	}
	
	function update_zip($zip_id, $zip_value){
		$this->db->where('id',$zip_id);
		$this->db->update('zipcodes',array('zip'=>$zip_value));
	}
	
	function fetch_array($rezult,$column_name){
		$the_return = array();
		foreach($rezult as $row) $the_return[$row->id]=$row->$column_name;
		return $the_return;
	}
	
	
	
	function update_country($data,$id){
		$this->db->where('id',$id);
		$this->db->update('countries',$data);
	}
	
	function update_language($data,$id){
		$this->db->where('id',$id);
		$this->db->update('languages',$data);
	}	
	
	function check_import($country_id){
		$this->db->where('id',$country_id);
		$row = $this->db->get('countries')->row();
		if($row->imported=='no') return true;
		else return false;
	}

	function insert_csv($result,$country_id){
		$skip_districts=false;
		$skip_zipcodes=false;
		foreach($result as $columns){
			foreach($columns as $column=>$value){ 		
				
				//add cities
				if($column=='name'){
					if($this->check_duplicate_city($value,$country_id)){
						$this->db->insert('cities',array('name'=>$value,'country_id'=>$country_id));
						$city_id = $this->db->insert_id();
						
					}
					else { $skip_districts = true; $skip_zipcodes=true; continue; }
				}
			
				//add districts	
				if($column=='districts'){ 
					if($skip_districts) {$skip_districts=false;continue;}
					$districts_array = array();
					$districts = explode('##',$value);
					if(count($districts)>0){
						$nr_districts = 0;
						foreach($districts as $district){
							//$this->db->where('name',$district);
							//$query = $this->db->get('districts');
							
							//if($query->num_rows()==0) { 
								$this->db->insert('districts',array('name'=>$district,'city_id'=>$city_id));
								$district_id = $this->db->insert_id();
								// $this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));
								$districts_array[$nr_districts] = $district_id;
								$nr_districts++;
								
							//}
							//else{
							//	$district_id = $query->row()->id;
							//	$this->db->insert('citi_district',array('city_id'=>$city_id,'district_id'=>$district_id));
							//	$districts_array[$nr_districts] = $district_id;
							//	$nr_districts++;
							//}
							 
						}
					}
					
				}
				//add zipcodes
				if($column=='zipcodes'&&$value!=""){	
					if($skip_zipcodes) {$skip_zipcodes=false;continue;}
					
					$zipcodes = explode('##',$value);
					

						
						$district_key = 0;
						
						foreach($zipcodes as $zips_by_district_separated_by_slash){
							if($zips_by_district_separated_by_slash!='no_zipcodes') {
								$zips_for_district = explode('|',$zips_by_district_separated_by_slash);
		
								foreach($zips_for_district as $the_zip){
									//check for ranges
									$ranges = explode('range',$the_zip);
									if(count($ranges)>1){
										$from = $ranges[0];
										$to	  = $ranges[1];
										for($from;$from<=$to;$from++){
											$this->db->insert('zipcodes',array('zip'=>$from,'city_id'=>$city_id));
											$zipcode_id = $this->db->insert_id();
											$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$districts_array[$district_key]));
										}
									 
									}
									else {
										if($the_zip!=""){
											$this->db->insert('zipcodes',array('zip'=>$the_zip,'city_id'=>$city_id));
											$zipcode_id = $this->db->insert_id();
											$this->db->insert('district_zipcode',array('zipcode_id'=>$zipcode_id,'district_id'=>$districts_array[$district_key]));
										}
									}
								}
							}
							$district_key++; 
						}	 
					
				
				}  
				unset($district_array);
			}	
		}
	}
	
	//CANDIDATES CRUD
	function insert_candidate($data){
		if( isset( $_FILES['photo']['name'] )&&  $_FILES['photo']['name']!=""){
			
			//update poza


			$file_ext 						= end( explode( '.', $_FILES['photo']['name'] ) );
			$config['upload_path'] 			=	 './uploads/candidates/temp';
			$config['allowed_types'] 		= '*';
			$config['max_size']				= '0';
			$poza_fara_extensie				= strtolower(url_title($this->input->post('name')));
			$config['file_name']			= $poza_fara_extensie.'_'.time().'.'.$file_ext;
			
			$this->load->library('upload', $config);		
			$file_state 					= $this->upload->do_upload('photo');
  
			if($file_state)
			{	

				$data['image']=$config['file_name'];
				$from = $config['upload_path']."/".$config['file_name'];
				$file_ext 					 = end( explode( '.', $data['image']) );

				$this->load->library('image_lib');
				 
				$configBig 					 = array();
				$configBig['image_library']  = 'gd2';
				$configBig['source_image']   = $from;
				$configBig['new_image']		 = './uploads/candidates';
				$configBig['maintain_ratio'] = TRUE;
				$configBig['create_thumb']   = FALSE;
				$configBig['quality']		 = '100%';
				$configBig['width'] 		 = 550;
				$configBig['height'] 		 = 330;
				$this->image_lib->initialize($configBig); 
				$this->image_lib->resize();
				$this->image_lib->clear();
				unset($configBig);

				unlink($from);
			}
		}
		  
		unset($data['send']);
		unset($data['_wysihtml5_mode']);
		unset($data['typeahead']);
		$this->db->insert('candidates',$data);
	}
	
	function update_candidate($candidate_id,$data){
		if( isset( $_FILES['photo']['name'] )&&  $_FILES['photo']['name']!=""){
			
			$image = $this->db->get_where('candidates',array('id'=>$candidate_id))->row()->image;

			if($image){
				if(file_exists('./uploads/candidates/'.$image)) {
					unlink('./uploads/candidates/'.$image);	
				}
			}
			//update image


			$file_ext 						= end( explode( '.', $_FILES['photo']['name'] ) );
			$config['upload_path'] 			= './uploads/candidates/temp';
			$config['allowed_types'] 		= '*';
			$config['max_size']				= '0';
			$poza_fara_extensie				= strtolower(url_title($this->input->post('name')));
			$config['file_name']			= $poza_fara_extensie.'_'.time().'.'.$file_ext;
			
			$this->load->library('upload', $config);		
			$file_state 					= $this->upload->do_upload('photo');
  
			if($file_state)
			{	

				$data['image']=$config['file_name'];
				$from = $config['upload_path']."/".$config['file_name'];
				$file_ext 					 = end( explode( '.', $data['image']) );

				$this->load->library('image_lib');
				 
				$configBig 					 = array();
				$configBig['image_library']  = 'gd2';
				$configBig['source_image']   = $from;
				$configBig['new_image']		 = './uploads/candidates';
				$configBig['maintain_ratio'] = TRUE;
				$configBig['create_thumb']   = FALSE;
				$configBig['quality']		 = '100%';
				$configBig['width'] 		 = 550;
				$configBig['height'] 		 = 330;
				$this->image_lib->initialize($configBig); 
				$this->image_lib->resize();
				$this->image_lib->clear();
				unset($configBig);

				unlink($from);
			} 
		}
		  
		unset($data['send']);
		unset($data['_wysihtml5_mode']);
		unset($data['typeahead']);
		
		$this->db->where('id',$candidate_id);	
		$this->db->update('candidates',$data);	
	
	}
	function get_candidate($id){
		return $this->db->get_where('candidates',array('id'=>$id))->row();
	}
	function get_district($id){
		return $this->db->get_where('districts',array('id'=>$id))->row();
	}	
	//pages
	function search_array($array, $key, $value){
		$results = array();
	 
		if (is_array($array))
		{
			if (isset($array[$key]) && $array[$key] == $value)
				$results[] = $array;

			foreach ($array as $subarray)
				$results = array_merge($results, $this->search_array($subarray, $key, $value));
		}

		return $results;
	}
	function get_page($page_id){
		$page 		 		  = $this->db->get_where('pages',array('id'=>$page_id))->row_array();
		$page['translations'] = $this->db->get_where('page_translations',array('page_id'=>$page_id))->result_array();
		return $page;		
	}
	function update_page($page_id,$data,$count_languages){
	
		 
		
		$this->db->where('id',$page_id);	
		$this->db->update('pages',array('general_name'=>$data['general_name']));
			
		for($i=1;$i<=$count_languages;$i++){
		
			$this->db->where('page_id',$page_id);
			$this->db->where('language_id',$i);
			$rez = $this->db->get('page_translations');
			if($rez->num_rows()==0){
				$insert = array(
					'language_id' 		=> $i,
					'page_id'			=> $page_id,
					'anchor'			=> $data['anchor'.$i],
					'slug'				=> strtolower(url_title($data['slug'.$i])),
					'content'			=> $data['content'.$i],
					'content_2'			=> $data['content_2'.$i],
					'content_3'			=> $data['content_3'.$i],
					'meta_title'		=>	$data['meta_title'.$i],
					'meta_description'	=>	$data['meta_description'.$i],
					'meta_keywords'		=>	$data['meta_keywords'.$i]
				);
				if($data['anchor'.$i] || $data['slug'.$i] || $data['content'.$i] || $data['content_2'.$i] || $data['content_3'.$i] || $data['meta_title'.$i] || $data['meta_description'.$i]|| $data['meta_keywords'.$i])
				$this->db->insert('page_translations',$insert);
			
			}
			else{ 
				$update = array(
					'anchor'			=> $data['anchor'.$i],
					'slug'				=> strtolower(url_title($data['slug'.$i])),
					'content'			=> $data['content'.$i],
					'content_2'			=> $data['content_2'.$i],
					'content_3'			=> $data['content_3'.$i],
					'meta_title'		=>	$data['meta_title'.$i],
					'meta_description'	=>	$data['meta_description'.$i],
					'meta_keywords'		=>	$data['meta_keywords'.$i]
				);
				$this->db->where('page_id',$page_id);
				$this->db->where('language_id',$i);				
				$this->db->update('page_translations',$update);
			}
		}
		
	 
	}
	function get_translations(){
		//$page 		 		  = $this->db->get_where('pages',array('id'=>$page_id))->row_array();
		$page['translations'] = $this->db->get_where('translations')->result_array();
		return $page;		
	}
	function update_translations($data,$count_languages){
	
		
			
		for($i=1;$i<=$count_languages;$i++){
		
			$this->db->where('language_id',$i);
			$rez = $this->db->get('translations');
			if($rez->num_rows()==0){
				$insert = array(
					'language_id' 						=> $i,
					'hero_title'						=> $data['hero_title'.$i],
					'hero_subtitle'						=> $data['hero_subtitle'.$i],
					'search_placeholder'				=> $data['search_placeholder'.$i],
					'promise_to_vote_intro' 			=> $data['promise_to_vote_intro'.$i],
					'point_charter' 					=> $data['point_charter'.$i],
					'digital_rights' 					=> $data['digital_rights'.$i],
					'name_placeholder'					=> $data['name_placeholder'.$i],
					'country_placeholder'				=> $data['country_placeholder'.$i],
					'promise_to_vote'					=> $data['promise_to_vote'.$i],
					'promised_to_vote'					=> $data['promised_to_vote'.$i],
					'election_promise_title'			=> $data['election_promise_title'.$i],
					'election_promise_content'			=> $data['election_promise_content'.$i],
					'their_election_promise'			=> $data['their_election_promise'.$i],
					'their_election_promise_content'	=> $data['their_election_promise_content'.$i],
					'their_election_promise_button'		=> $data['their_election_promise_button'.$i],
					'sign_the_charter'					=> $data['sign_the_charter'.$i],
					'what_can_i_do'						=> $data['what_can_i_do'.$i],
					'what_can_i_do_content'				=> $data['what_can_i_do_content'.$i],
					'you_care'							=> $data['you_care'.$i],
					'read_principles'					=> $data['read_principles'.$i],
					'signups_map'						=> $data['signups_map'.$i],
					'signups_map_desc'					=> $data['signups_map_desc'.$i],
					'recent_candidates'					=> $data['recent_candidates'.$i],
					'candidates'						=> $data['candidates'.$i],
					'spread_news'						=> $data['spread_news'.$i],
					'download_kit'						=> $data['download_kit'.$i],
					'footer_copyright'					=> $data['footer_copyright'.$i],
					'results_for'						=> $data['results_for'.$i],
					'results_section'					=> $data['results_section'.$i],
					'meta_title'						=> $data['meta_title'.$i],
					'meta_description'					=> $data['meta_description'.$i],
					'meta_keywords'						=> $data['meta_keywords'.$i],
					'success_message'					=> $data['success_message'.$i],
					'duplicate_email'					=> $data['duplicate_email'.$i],
					'please_validate_email'				=> $data['please_validate_email'.$i],
					//'fields_required'					=> $data['fields_required'.$i],				
					'validation_error'					=> $data['validation_error'.$i],
					'receive_newsletter'				=> $data['receive_newsletter'.$i],
					'text_below_search_cms'				=> $data['text_below_search_cms'.$i],
					'country_required'					=> $data['country_required'.$i],
					'email_required'					=> $data['email_required'.$i],
					'name_required'						=> $data['name_required'.$i]
					
					
										
					
				);
				if(	
					$data['hero_title'.$i] || 
					$data['hero_title'.$i] || 
					$data['search_placeholder'.$i] || 
					$data['promise_to_vote_intro'.$i] || 
					$data['point_charter'.$i] || 
					$data['digital_rights'.$i] || 
					$data['name_placeholder'.$i] || 
					$data['country_placeholder'.$i] || 
					$data['promise_to_vote'.$i] || 
					$data['promised_to_vote'.$i] || 				
					$data['election_promise_title'.$i] || 
					$data['election_promise_content'.$i] || 
					$data['their_election_promise'.$i] || 
					$data['their_election_promise_content'.$i] || 
					$data['their_election_promise_button'.$i] || 
					$data['sign_the_charter'.$i] ||
					$data['what_can_i_do'.$i] || 
					$data['what_can_i_do_content'.$i] || 
					$data['you_care'.$i] || 
					$data['read_principles'.$i] || 
					$data['signups_map'.$i] || 
					$data['signups_map_desc'.$i] || 
					$data['recent_candidates'.$i] || 
					$data['candidates'.$i] || 
					$data['spread_news'.$i] ||  
					$data['download_kit'.$i] || 
					$data['footer_copyright'.$i] ||
					$data['results_for'.$i] ||  
					$data['results_section'.$i] ||  
					$data['meta_title'.$i] ||  
					$data['meta_description'.$i] ||  
					$data['meta_keywords'.$i] ||
					$data['success_message'.$i] ||  
					$data['duplicate_email'.$i] ||  
					$data['please_validate_email'.$i] ||  
					//$data['fields_required'.$i] ||
					$data['validation_error'.$i] ||
					$data['receive_newsletter'.$i] ||
					$data['text_below_search_cms'.$i] ||
					$data['name_required'.$i] ||
					$data['email_required'.$i] ||
					$data['country_required'.$i]
				)
				$this->db->insert('translations',$insert);
			
			}
			else{ 
				$update = array(
					'hero_title'						=> $data['hero_title'.$i],
					'hero_subtitle'						=> $data['hero_subtitle'.$i],
					'search_placeholder'				=> $data['search_placeholder'.$i],
					'promise_to_vote_intro' 			=> $data['promise_to_vote_intro'.$i],
					'point_charter' 					=> $data['point_charter'.$i],
					'digital_rights' 					=> $data['digital_rights'.$i],
					'name_placeholder'					=> $data['name_placeholder'.$i],
					'country_placeholder'				=> $data['country_placeholder'.$i],
					'promise_to_vote'					=> $data['promise_to_vote'.$i],
					'promised_to_vote'					=> $data['promised_to_vote'.$i],
					'election_promise_title'			=> $data['election_promise_title'.$i],
					'election_promise_content'			=> $data['election_promise_content'.$i],
					'their_election_promise'			=> $data['their_election_promise'.$i],
					'their_election_promise_content'	=> $data['their_election_promise_content'.$i],
					'their_election_promise_button'		=> $data['their_election_promise_button'.$i],
					'sign_the_charter'					=> $data['sign_the_charter'.$i],
					'what_can_i_do'						=> $data['what_can_i_do'.$i],
					'what_can_i_do_content'				=> $data['what_can_i_do_content'.$i],
					'you_care'							=> $data['you_care'.$i],
					'read_principles'					=> $data['read_principles'.$i],
					'signups_map'						=> $data['signups_map'.$i],
					'signups_map_desc'					=> $data['signups_map_desc'.$i],
					'recent_candidates'					=> $data['recent_candidates'.$i],
					'candidates'						=> $data['candidates'.$i],
					'spread_news'						=> $data['spread_news'.$i],
					'download_kit'						=> $data['download_kit'.$i],
					'footer_copyright'					=> $data['footer_copyright'.$i],
					'results_for'						=> $data['results_for'.$i],
					'results_section'					=> $data['results_section'.$i],
					'meta_title'						=> $data['meta_title'.$i],
					'meta_description'					=> $data['meta_description'.$i],
					'meta_keywords'						=> $data['meta_keywords'.$i],
					'success_message'					=> $data['success_message'.$i],
					'duplicate_email'					=> $data['duplicate_email'.$i],
					'please_validate_email'				=> $data['please_validate_email'.$i],
					//'fields_required'					=> $data['fields_required'.$i],
					'validation_error'					=> $data['validation_error'.$i],
					'receive_newsletter'				=> $data['receive_newsletter'.$i],
					'text_below_search_cms'				=> $data['text_below_search_cms'.$i],
					'country_required'					=> $data['country_required'.$i],
					'email_required'					=> $data['email_required'.$i],
					'name_required'						=> $data['name_required'.$i]
				);

				$this->db->where('language_id',$i);				
				$this->db->update('translations',$update);
			}
		}
		
	
	}	
	function translations(){
	 
	
	
	}
	
	function check_duplicate_name($name){
		$this->db->where('name',$name);
		$query = $this->db->get('candidates');
		if($query->num_rows()==0) return true;
		else return false;
	}
	function check_duplicate_name_on_update($name){
		$this->db->where('name',$name);
		$query = $this->db->get('candidates');
		//we set num_rows == 1; on update there is already 1 record inserted. this record!
		if($query->num_rows()==1) return true;
		else return false;	
	
	}
	function check_duplicate_city($name,$country_id){
		$this->db->where('name',$name);
		$this->db->where('country_id',$country_id);
		$query = $this->db->get('cities'); 
		if($query->num_rows()==0) return true;
		else return false;	 
	}
	function check_duplicate_district($name,$district_id){
		$district = $this->get_district($district_id);
		
		$this->db->where('city_id',$district->city_id);
		$this->db->where('name',$name);
		$this->db->where_not_in('id',array($district_id));
		$query = $this->db->get('districts'); 
		if($query->num_rows()>0) return true;
		else return false;	
	}
	
	function check_duplicate_zip($name,$zip_id){
		$zip = $this->get_zip($zip_id);
		
		
		$this->db->where('city_id',$zip->city_id);
		$this->db->where('zip',$name);
		$this->db->where_not_in('id',array($zip_id));  
		$query = $this->db->get('zipcodes'); 
		if($query->num_rows()>0) return true;
		else return false;	 
		
	}
	
	function check_translates($language_id){
	
		$errors = "";
	
	
		$errors_trans="";
		$language=$this->db->get_where('languages',array('id'=>$language_id))->row();
		$translations = $this->db->get_where('translations',array('language_id'=>$language_id));
		if($translations->num_rows()==1){
			$fields = $this->db->field_data('translations');
				
				foreach ($fields as $field)
				{
					$val = $field->name;
					if($translations->row()->$val =="") $errors_trans .= "<strong>".ucfirst(str_replace("_"," ",$field->name))."</strong> from the <a target='_blank' href='".base_url()."admin/translations'>Translations page</a><br/>";

				} 
		
		}
		else $errors_trans = "Please complete the translations for ".strtoupper($language->name)." from the <a target='_blank' href='".base_url()."admin/translations'>Translations page</a><br/>";
		$errors.=$errors_trans;
		
		
		$page_errors_trans="";
		$pages = $this->db->get('pages')->result();
		$page_translations = $this->db->get_where('page_translations',array('language_id'=>$language_id))->result();
		$fields = $this->db->field_data('page_translations');

			
		foreach($pages as $page){
			$t=0;
			foreach($page_translations as $pt){
			
				if($page->id==$pt->page_id) { 
				$t=1;
				
					foreach ($fields as $field)
							{
								$val = $field->name;
								
								if($pt->$val=="") {
									
									$page_errors_trans .= "<strong>  $page->general_name </strong>  from the <a target='_blank'  href='".base_url()."admin/pages/$page->id'>$page->general_name CMS</a><br/>";
									break;
								}

							} 
					
				} 
			 
			}
			if($t==0)$page_errors_trans .= "<strong>  $page->general_name </strong>  from the <a target='_blank'  href='".base_url()."admin/pages/$page->id'>$page->general_name CMS</a><br/>";
		
		}
		 
		
		
		$errors .=$page_errors_trans;
		if($language->support_kit=="") $errors .="Please upload the support kit for $language->name from the  <a target='_blank'  href='".base_url()."admin/support_kits'>Support kits page</a>";
		
		 
		 
		if($errors!="")  {
			$errors ="<h2 style='margin-top:0'>You cannot activate $language->name until all the fields below are translated</h2>".$errors;
			return $errors;
		
		}
		else
		return false;
	}
	
	function clear_imported_data($country_id){
		$cities    = $this->db->get_where('cities',array('country_id'=>$country_id,'type'=>'imported'))->result();
		foreach($cities as $city){

			$city_districts = $this->db->get_where(('districts'),array('city_id'=>$city->id))->result();
			foreach($city_districts as $district){
					
				//remove candidate`s district associations
				$this->db->where('district_id',$district->id);
				$this->db->update('candidates',array('district_id'=>'0','status'=>'inactiv'));
				
				//delete all district zipcodes associations
				$this->db->where('district_id',$district->id);
				$this->db->delete('district_zipcode');
				
			}
			
			//delete all zipcodes for this city		
			$this->db->where('city_id',$city->id);
			$this->db->delete('zipcodes');
			
			//delete all districts for this city		
			$this->db->where('city_id',$city->id);
			$this->db->delete('districts');
			
			//remove candidate`s city associations
			$this->db->where('city_id',$city->id);
			$this->db->update('candidates',array('city_id'=>'0','status'=>'inactiv'));
		}

		
		//delete all cities
		$this->db->where('country_id',$country_id);
		$this->db->delete('cities');
		
			$this->db->where('id',$country_id);
			$this->db->update('countries',array('imported'=>'no'));
					
		
	}
}